-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 07, 2018 at 09:58 PM
-- Server version: 10.2.14-MariaDB-10.2.14+maria~xenial
-- PHP Version: 7.2.4-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `git_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `qb_change`
--

CREATE TABLE `qb_change` (
  `id` int(11) NOT NULL,
  `operation` text NOT NULL,
  `db_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `qb_change`
--

INSERT INTO `qb_change` (`id`, `operation`, `db_id`) VALUES
(77, 'DROP+TABLE+%60fffff%60', 9);

-- --------------------------------------------------------

--
-- Table structure for table `qb_commit`
--

CREATE TABLE `qb_commit` (
  `id` int(11) NOT NULL,
  `code` text NOT NULL,
  `data` text CHARACTER SET utf8 NOT NULL,
  `db_id` int(11) NOT NULL,
  `author` text NOT NULL,
  `executed` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `qb_commit`
--

INSERT INTO `qb_commit` (`id`, `code`, `data`, `db_id`, `author`, `executed`) VALUES
(21, '5af0d697ee91833', '[\"ALTER TABLE `ddd`  ADD `33` INT NOT NULL  AFTER `id`;\",\"ALTER TABLE `ddd`  ADD `3333` INT NOT NULL  AFTER `id`;\"]', 11, 'Javier Coro', 1),
(22, '5af0d697ee91d', '[\"ALTER TABLE `ddd`  ADD `33` INT NOT NULL  AFTER `id`;\",\"ALTER TABLE `ddd`  ADD `3333` INT NOT NULL  AFTER `id`;\"]', 11, 'Javier Coro', 1),
(23, '5af101fd560ba', '[\"ALTER TABLE `dd` DROP `qq`;\"]', 11, 'Javier Coro', 1),
(24, '5af1031c069d8', '[\"ALTER TABLE `dd`  ADD `ddd` INT NOT NULL  AFTER `asdasd`,  ADD `www` INT NOT NULL  AFTER `ddd`,  ADD `qqq` INT NOT NULL  AFTER `www`;\",\"ALTER TABLE `dd`  ADD `dd` INT NOT NULL  AFTER `asdasd`,  ADD `qqq` INT NOT NULL  AFTER `dd`;\"]', 11, 'Javier Coro', 1),
(25, '5af10433b2a0f', '[\"INSERT INTO `dd` (`asdasd`, `dd`, `qqq`) VALUES (\'33\', \'11\', \'33\')\",\"ALTER TABLE `dd` DROP `qqq`;\",\"ALTER TABLE `dd` DROP `dd`;\"]', 11, 'Javier Coro', 1);

-- --------------------------------------------------------

--
-- Table structure for table `qb_db`
--

CREATE TABLE `qb_db` (
  `id` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `qb_db`
--

INSERT INTO `qb_db` (`id`, `name`) VALUES
(9, 'fffff'),
(11, 'aaa'),
(12, 'dd'),
(13, 'ddd');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `qb_change`
--
ALTER TABLE `qb_change`
  ADD PRIMARY KEY (`id`),
  ADD KEY `change_db__fk` (`db_id`);

--
-- Indexes for table `qb_commit`
--
ALTER TABLE `qb_commit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `commit_db__fk` (`db_id`);

--
-- Indexes for table `qb_db`
--
ALTER TABLE `qb_db`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `qb_change`
--
ALTER TABLE `qb_change`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT for table `qb_commit`
--
ALTER TABLE `qb_commit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `qb_db`
--
ALTER TABLE `qb_db`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `qb_change`
--
ALTER TABLE `qb_change`
  ADD CONSTRAINT `change_db__fk` FOREIGN KEY (`db_id`) REFERENCES `qb_db` (`id`);

--
-- Constraints for table `qb_commit`
--
ALTER TABLE `qb_commit`
  ADD CONSTRAINT `commit_db__fk` FOREIGN KEY (`db_id`) REFERENCES `qb_db` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
