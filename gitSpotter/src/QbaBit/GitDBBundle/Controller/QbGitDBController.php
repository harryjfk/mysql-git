<?php

namespace QbaBit\GitDBBundle\Controller;


use QbaBit\CoreBundle\Core\Controller\QbaBitBaseController;
use QbaBit\GitDBBundle\Entity\QbCommit;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use QbaBit\CoreBundle\Core\Traits\FormErrorMessagesTrait;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

/**
 * @Route("/")
 */
class QbGitDBController extends QbaBitBaseController
{
    use FormErrorMessagesTrait;

    public function getFileContents($function)
    {


        $file = $function;
        try {
            return Yaml::parse(file_get_contents($file));
        } catch (ParseException $e) {
            printf("Unable to parse the YAML string: %s", $e->getMessage());
        }
        return null;
    }

    public function setFileContent($function, $data)
    {
        $yaml = Yaml::dump($data);
        file_put_contents($function, $yaml);
    }

    /**
     * @Route("/push", name="qb_git_db_push")
     */
    public function pushAction(): Response
    {
        $this_bundle = $this->get("qbabit.core.bundle_utils")->getQbaBitBundles()["QbaBitGitDBBundle"];

        $file = $this_bundle->getPath() . "/Resources/config/parameters.yml";
        $content = $this->getFileContents($file);
        $em = $this->get("doctrine.orm.default_entity_manager");
        $default_db = $em->getRepository("QbaBitGitDBBundle:QbDB")->findOneBy(array("name" => $content["parameters"]["git_default_db"]));
        $data = array();
        foreach ($default_db->getCommits() as $commit)
            $data[] = array("code" => $commit->getCode(), "data" => $commit->getData(), "author" => $commit->getAuthor());


        $export_dir = $this->getParameter("kernel.project_dir") . "/public/data/";
        if(is_dir($export_dir))
            mkdir($export_dir);
        $export_file = $export_dir . $default_db->getName();
        file_put_contents($export_file, json_encode($data));
        $zip = new \ZipArchive();
        $zipName = $export_dir . $default_db->getName() . ".zip";
        $zip->open($zipName, \ZipArchive::CREATE);
        $zip->addFromString(basename($export_file), file_get_contents($export_file));
        $zip->close();

        $response = new Response(file_get_contents($zipName));
        $response->headers->set('Content-Type', 'application/zip');
        $response->headers->set('Content-Disposition', 'attachment;filename="git_' . $default_db->getName() . ".zip" . '"');
        $response->headers->set('Content-length', filesize($zipName));

        return $response;
    }

    /**
     * @Route("/commit/{id}", name="qb_git_db_commit")
     */
    public function commitAction(Request $request,$id): Response
    {
        $this_bundle = $this->get("qbabit.core.bundle_utils")->getQbaBitBundles()["QbaBitGitDBBundle"];
        $file = $this_bundle->getPath() . "/Resources/config/parameters.yml";
        $content = $this->getFileContents($file);
        $em = $this->get("doctrine.orm.default_entity_manager");
        $default_db = $em->getRepository("QbaBitGitDBBundle:QbDB")->findOneBy(array("name" => $content["parameters"]["git_default_db"]));

        if($id==-1)
        {
             if ($default_db->getChanges()->count() == 0)
                return new JsonResponse(array("return" => "0", "msg" => "No hay nada para commit"));
            else {
                $operations = array();
                foreach ($default_db->getChanges() as $change) {
                    $operations[] = $change->getOperation();

                    $em->remove($change);
                }
                $default_db->getChanges()->clear();
                $commit = new QbCommit();
                $commit->setData(json_encode($operations));
                $commit->setDB($default_db);
                $commit->setCode(uniqid());
                $commit->setExecuted(true);
                $commit->setAuthor($content["parameters"]["git_user"]);
                $em->persist($commit);
                $em->flush($commit);
                return new JsonResponse(array("return" => "1", "msg" => "Se completo con exito el commit"));

            }
        }
        else
        {
            $commit = $em->getRepository("QbaBitGitDBBundle:QbCommit")->find($id);
            $operations = json_decode( $commit->getData());
            $errors = array();
            foreach ($operations as $operation)
            {
                try
                {
                    $db = $default_db->getName();
                    $stmt=  $em->getConnection()->executeUpdate("use $db;".$operation);
                }
                catch (\Exception $e)
                {
                    $errors[]=$operation;
                }

            }

            $str = "";
            if(count($errors)==0)
            {
                $em->getConnection()->executeUpdate("use git_db;");
                $str="Se completo con exito el commit";
                $commit->setExecuted(1);
                $em->persist($commit);
                $em->flush();
            }

            else
            {
                $str="Existen errores al momento de ejecutar las siguientes consultas:\n";
                foreach ($errors as $error)
                    $str[]=$error."\n";
            }
            return new JsonResponse(array("return" => count($errors)>0?"0":"1", "msg" => $str));


        }



    }

    /**
     * @Route("/", name="qb_git_db_index")
     */
    public function indexAction(Request $request): Response
    {
        $this_bundle = $this->get("qbabit.core.bundle_utils")->getQbaBitBundles()["QbaBitGitDBBundle"];
        $file = $this_bundle->getPath() . "/Resources/config/parameters.yml";
        $content = $this->getFileContents($file);
        $data = $content["parameters"];
        $em  =  $this->get("doctrine.orm.default_entity_manager");
        $default_db =$em->getRepository("QbaBitGitDBBundle:QbDB")->findOneBy(array("name" => $content["parameters"]["git_default_db"]));
        $data["git_default_db"] = $default_db->getId();

        //  $dbs = $this->get("doctrine.orm.default_entity_manager")->getRepository("QbaBitGitDBBundle:QbDB")->findAll();
          $c = $this->createForm("QbaBit\GitDBBundle\Form\QbAuthorType", $data, array("method" => "POST"));
        $c->handleRequest($request);
        if( $c->getData()["git_default_db"]!=null && is_object($c->getData()["git_default_db"]))
            $default_db =  $c->getData()["git_default_db"];

        if ($c->isSubmitted() && $c->isValid()) {
            if ($content["parameters"]["git_user"] != $c->getData()["git_user"] || $content["parameters"]["git_mail"] != $c->getData()["git_mail"]|| $content["parameters"]["git_default_db"] != $c->getData()["git_default_db"]->getName()) {
                $content["parameters"] = $c->getData();
                $content["parameters"]["git_default_db"] = $content["parameters"]["git_default_db"]->__toString();
                $this->setFileContent($file, $content);
            }
            $f = $c->getData()["pull"];
            if ($f != null) {
                $export_dir = $this->getParameter("kernel.project_dir") . "/public/data/temp/";
              //  if(is_dir($export_dir))
              //      mkdir($export_dir);

                $f->move($export_dir, $f->getClientOriginalName());
                $zip = new \ZipArchive();
                $zipName = $export_dir . $f->getClientOriginalName();
                $zip->open($zipName, \ZipArchive::CREATE);
                $zip->extractTo($export_dir);

                $git_db= $content["parameters"]["git_default_db"];
                if(is_object($git_db))
                    $git_db = $git_db->__toString();
                $cont = file_get_contents($export_dir.$git_db);
                $zip->close();
                $cont_conv = json_decode($cont);
                foreach ($cont_conv as $value)
                {
                    $commit = $em->getRepository("QbaBitGitDBBundle:QbCommit")->findOneBy(array("code"=>$value->code));
                    if($commit==null)
                    {
                        $commit = new QbCommit();
                        $commit->setCode($value->code);
                        $commit->setAuthor($value->author);
                        $commit->setExecuted(0);
                        $commit->setData($value->data);
                        $commit->setDB($default_db);
                        $em->persist($commit);
                        $em->flush();
                    }
                }
            }
        }

        return $this->renderQbabitScope("@QbaBitGitDB/QbGitDB/index.html.twig", "backend", array("db" => $default_db, "data" => $data, "form" => $c->createView()));

    }


}
