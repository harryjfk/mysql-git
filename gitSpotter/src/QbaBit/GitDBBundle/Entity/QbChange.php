<?php
/**
 * Created by PhpStorm.
 * User: cronk
 * Date: 3/25/18
 * Time: 5:52 PM
 */

namespace QbaBit\GitDBBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use QbaBit\CoreBundle\Core\Classes\ArrayGetter;
use QbaBit\CoreBundle\Core\Traits\FileUploadable;
use QbaBit\CoreBundle\Core\Traits\Identificable;
use QbaBit\CoreBundle\Core\Traits\Nameable;
use QbaBit\TemplateBundle\Libs\Embeddable\QbTemplateConfiguration;
use QbaBit\TemplateBundle\Libs\Embeddable\QbTemplateSelectedConfiguration;
use QbaBit\TemplateBundle\Libs\Interfaces\QbaBitTemplateInterface;
use QbaBit\TemplateBundle\Entity\QbTemplateCategory;

/**
 * QbTemplate
 *
 * @ORM\Table(name="qb_change")
 * @ORM\Entity(repositoryClass="QbaBit\GitDBBundle\Repository\QbChangeRepository")
 */
class QbChange extends ArrayGetter
{
    /**
     * @return string
     */
    public function getOperation()
    {
        return urldecode($this->operation);
    }

    /**
     * @param string $operation
     * @return QbChange
     */
    public function setOperation($operation)
    {
        $this->operation = $operation;
        return $this;
    }

    use Identificable ;
    /**
     * @var string
     *
     * @ORM\Column(name="operation", type="string", nullable=false)
     */
    private $operation;




    /**
     * @ORM\ManyToOne(targetEntity="QbaBit\GitDBBundle\Entity\QbDB", inversedBy="changes", cascade={"all"}, fetch="LAZY")
     */
    private $db;
    /**
     * @return QbDB
     */
    public function getDB()
    {
        return $this->db;
    }

    /**
     * @param QbDB $db
     * @return QbChange
     */
    public function setDB(QbDB $db)
    {
        $this->db = $db;
        return $this;
    }

}