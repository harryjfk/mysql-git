<?php
/**
 * Created by PhpStorm.
 * User: cronk
 * Date: 3/25/18
 * Time: 5:52 PM
 */

namespace QbaBit\GitDBBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use QbaBit\CoreBundle\Core\Classes\ArrayGetter;
use QbaBit\CoreBundle\Core\Traits\FileUploadable;
use QbaBit\CoreBundle\Core\Traits\Identificable;
use QbaBit\CoreBundle\Core\Traits\Nameable;
use QbaBit\TemplateBundle\Libs\Embeddable\QbTemplateConfiguration;
use QbaBit\TemplateBundle\Libs\Embeddable\QbTemplateSelectedConfiguration;
use QbaBit\TemplateBundle\Libs\Interfaces\QbaBitTemplateInterface;
use QbaBit\TemplateBundle\Entity\QbTemplateCategory;

/**
 * QbTemplate
 *
 * @ORM\Table(name="qb_db")
 * @ORM\Entity(repositoryClass="QbaBit\GitDBBundle\Repository\QbDBRepository")
 */
class QbDB extends ArrayGetter
{

    use Identificable,Nameable ;


    /**
     * @ORM\OneToMany(targetEntity="QbaBit\GitDBBundle\Entity\QbChange", mappedBy="db", cascade={"persist", "remove", "merge"}, orphanRemoval=false)
     */
    protected $changes;
    /**
     * @ORM\OneToMany(targetEntity="QbaBit\GitDBBundle\Entity\QbCommit", mappedBy="db", cascade={"persist", "remove", "merge"}, orphanRemoval=false)
     */
    protected $commit;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->changes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->commit = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add image.
     *
     * @param \QbaBit\GitDBBundle\Entity\QbChange $change
     *
     * @return QbDB
     */
    public function addChange(\QbaBit\GitDBBundle\Entity\QbChange $change)
    {
        $this->changes[] = $change;

        return $this;
    }
    /**
     * Add image.
     *
     * @param \QbaBit\GitDBBundle\Entity\QbCommit $change
     *
     * @return QbDB
     */
    public function addCommit(\QbaBit\GitDBBundle\Entity\QbCommit $commit)
    {
        $this->commit[] = $commit;

        return $this;
    }
    /**
     * Remove image.
     *
     * @param \QbaBit\GitDBBundle\Entity\QbChange $image
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeChange(\QbaBit\GitDBBundle\Entity\QbChange $change)
    {
        return $this->changes->removeElement($change);
    }
    /**
     * Remove image.
     *
     * @param \QbaBit\GitDBBundle\Entity\QbCommit $image
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCommit(\QbaBit\GitDBBundle\Entity\QbCommit $commit)
    {
        return $this->commit->removeElement($commit);
    }
    /**
     * Get images.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChanges()
    {
        return $this->changes;
    }
    /**
     * Get images.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommits()
    {
        return $this->commit;
    }

}