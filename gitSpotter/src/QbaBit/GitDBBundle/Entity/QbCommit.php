<?php
/**
 * Created by PhpStorm.
 * User: cronk
 * Date: 3/25/18
 * Time: 5:52 PM
 */

namespace QbaBit\GitDBBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use QbaBit\CoreBundle\Core\Classes\ArrayGetter;
use QbaBit\CoreBundle\Core\Traits\FileUploadable;
use QbaBit\CoreBundle\Core\Traits\Identificable;
use QbaBit\CoreBundle\Core\Traits\Nameable;
use QbaBit\TemplateBundle\Libs\Embeddable\QbTemplateConfiguration;
use QbaBit\TemplateBundle\Libs\Embeddable\QbTemplateSelectedConfiguration;
use QbaBit\TemplateBundle\Libs\Interfaces\QbaBitTemplateInterface;
use QbaBit\TemplateBundle\Entity\QbTemplateCategory;

/**
 * QbTemplate
 *
 * @ORM\Table(name="qb_commit")
 * @ORM\Entity(repositoryClass="QbaBit\GitDBBundle\Repository\QbCommitRepository")
 */
class QbCommit extends ArrayGetter
{

    use Identificable ;
    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", nullable=false)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="data", type="string", nullable=false)
     */
    private $data ;

    /**
     * @var string
     *
     * @ORM\Column(name="author", type="string", nullable=false)
     */
    private $author ;

    /**
     * @var integer
     *
     * @ORM\Column(name="executed", type="integer", nullable=false)
     */
    private $executed ;

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param string $author
     * @return QbCommit
     */
    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return integer
     */
    public function getExecuted()
    {
        return $this->executed;
    }

    /**
     * @param integer $executed
     * @return QbCommit
     */
    public function setExecuted($executed)
    {
        $this->executed = $executed;
        return $this;
    }
    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return QbCommit
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param string $data
     * @return QbCommit
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }





    /**
     * @ORM\ManyToOne(targetEntity="QbaBit\GitDBBundle\Entity\QbDB", inversedBy="commit", cascade={"all"}, fetch="EAGER")
     */
    private $db;
    /**
     * @return QbDB
     */
    public function getDB()
    {
        return $this->db;
    }

    /**
     * @param QbDB $db
     * @return QbChange
     */
    public function setDB(QbDB $db)
    {
        $this->db = $db;
        return $this;
    }

}