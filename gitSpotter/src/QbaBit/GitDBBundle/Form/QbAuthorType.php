<?php

namespace QbaBit\GitDBBundle\Form;

use Doctrine\ORM\EntityManager;


use QbaBit\TemplateBundle\Entity\QbTemplate;
use QbaBit\TemplateBundle\Entity\QbTemplateCategory;
use QbaBit\TemplateBundle\Entity\QbTemplateImages;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QbAuthorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('git_user', TextType::class, ['label'=>'Usuario',"attr"=>["class"=>"form-control"]])
            ->add('git_mail', TextType::class, ['label'=>'Correo',"attr"=>["class"=>"form-control"]])
            ->add('button', SubmitType::class, ['label'=>'Guardar'])
            ->add('git_default_db', EntityType::class, ["class"=>"QbaBit\GitDBBundle\Entity\QbDB","attr"=>["class"=>"form-control"],'label'=>'Base de datos'])
            ->add('commit', ButtonType::class, ['label'=>'Commit'])
            ->add('push', ButtonType::class, ['label'=>'Push'])
            ->add('pull', FileType::class, ['label'=>'Pull',"required"=>false])
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([


        ]);
    }
}
