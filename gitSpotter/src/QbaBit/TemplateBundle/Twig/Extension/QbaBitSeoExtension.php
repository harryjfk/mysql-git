<?php

namespace QbaBit\TemplateBundle\Twig\Extension;

class QbaBitSeoExtension extends \Twig_Extension
{
    /**
     * @var array
    */
    private static $dataSEO;

    public function __construct()
    {
        self::$dataSEO = ['title'=>'', 'metas'=>[]];
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'qba_bit_seo';
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('QbaBitSeoTitle', array($this, 'QbaBitSeoTitle')),
            new \Twig_SimpleFunction('QbaBitAddMeta', array($this, 'QbaBitAddMeta')),
            new \Twig_SimpleFunction('QbaBitKeyWords', array($this, 'QbaBitKeyWords')),
            new \Twig_SimpleFunction('QbaBitMetaDescription', array($this, 'QbaBitMetaDescription')),
            new \Twig_SimpleFunction('QbaBitRenderMetas', array($this, 'QbaBitRenderMetas')),
        );
    }

    /**
     * @param array meta values
     * @return string
    */
    private function parseMeta(array $metasData)
    {
        if(!count($metasData)){
          return;
        }

        $data = [];
        foreach ($metasData as $k=>$v)
        {
            if($k == 'wasRendered'){
                continue;
            }

            if($v){
                $data[] = sprintf('%s="%s"', $k, $v);
            }
        }

        if(!$data){
            return;
        }

        return sprintf('<meta %s />', implode(' ', $data));
    }

    /**
     * @param string $title if title is set the method works as setter otherwise works as getter
     * @return string|null
    */
    public function QbaBitSeoTitle($title = '')
    {
        if(!$title) {
            return self::$dataSEO['title'];
        }

        self::$dataSEO['title'] = $title;
    }

    /**
     * @param string $keywords if keywords is set the method works as setter otherwise works as getter
     * @return string|null
     */
    public function QbaBitKeyWords($keywords = '')
    {
        if(!$keywords) {
            if (array_key_exists('keywords', self::$dataSEO['metas'])) {
                self::$dataSEO['metas']['keywords']['wasRendered'] = true;
                return $this->parseMeta(self::$dataSEO['metas']['keywords']);
            }
        }
        else{
            $this->QbaBitAddMeta('keywords', $keywords);
        }
    }

    /**
     * @param string $description if description is set the method works as setter otherwise works as getter
     * @return string|null
     */
    public function QbaBitMetaDescription($description)
    {
        if(!$description) {
            if (array_key_exists('description', self::$dataSEO['metas'])) {
                self::$dataSEO['metas']['description']['wasRendered'] = true;
                return $this->parseMeta(self::$dataSEO['metas']['description']);
            }
        }
        else{
            $this->QbaBitAddMeta('description', $description);
        }
    }

    /**
     * @return string concatenated html metas
    */
    public function QbaBitRenderMetas()
    {
        $strMetas = '';
        foreach (self::$dataSEO['metas'] as $meta){
            if(!$meta['wasRendered']){
                $strMetas .= sprintf("%s\n", $this->parseMeta($meta));
                $meta['wasRendered'] = true;
            }
        }
        return $strMetas;
    }

    /**
     * @param string $name set attr name, ie: name="$name"
     * @param string $content set attr name, ie: content="$content"
     * @param string $property set property name, ie: property="$content"
    */
    public function QbaBitAddMeta($name, $content = '', $property = '')
    {
        $idMeta = sprintf('%s%s', $name, $property);
        if(array_key_exists($idMeta, self::$dataSEO['metas'])){
            return;
        }
        self::$dataSEO['metas'][$idMeta] = ['name'=>$name, 'content'=>$content, 'property'=>$property, 'wasRendered'=>false];
    }
}
