<?php

namespace QbaBit\TemplateBundle\Twig\Extension;

use QbaBit\CoreBundle\Core\Traits\ServiceContainer;
use Symfony\Component\DependencyInjection\ContainerInterface;

class QbaBitAssetsExtension extends \Twig_Extension
{

    use ServiceContainer;

    private static  $assets;

    public function __construct(ContainerInterface $container =null)
    {
        $this->container = $container;
        self::$assets = [
                'cssSource' => [],
                'jsSource' => []
            ];



    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'qba_bit_assets';
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('QbaBitAssetsAddCssSource', array($this, 'QbaBitAssetsAddCssSource')),
            new \Twig_SimpleFunction('QbaBitAssetsAddScriptSource', array($this, 'QbaBitAssetsAddScriptSource')),
            new \Twig_SimpleFunction('QbaBitAssetsRenderCssSources', array($this, 'QbaBitAssetsRenderCssSources')),
            new \Twig_SimpleFunction('QbaBitAssetsRenderScriptSource', array($this, 'QbaBitAssetsRenderScriptSource')),
            new \Twig_SimpleFunction('QbaBitAssetsRenderPostScriptSource', array($this, 'QbaBitAssetsRenderPostScriptSource')),
        );
    }

    private function parseOrderSources(array $listScripts)
    {
        $result = [];
        $end = [];
        foreach ($listScripts as $itm) {
            if (!$itm['end']) {
                $result[] = $itm['src'];
            } else {
                $end[] = $itm['src'];
            }
        }

        return array_merge($result, $end);
    }

    public function QbaBitAssetsAddCssSource($source, $attachAtTheEnd = false)
    {
        if (!array_key_exists($source, self::$assets['cssSource'])) {
            self::$assets['cssSource'][$source] = ['src' => $source,"active"=>false, 'end' => $attachAtTheEnd];

        }
    }

    public function QbaBitAssetsAddScriptSource($source, $attachAtTheEnd = false)
    {
        if (!array_key_exists($source, self::$assets['jsSource'])) {
            self::$assets['jsSource'][$source] = ["active"=>false,'src' => $source, 'end' => $attachAtTheEnd];
        }
    }

    public function QbaBitAssetsRenderCssSources($missing=false)
    {
        $str = '';
        foreach ($this->parseOrderSources(self::$assets['cssSource']) as $src)
        if(($missing==true && self::$assets['cssSource'][$src]["active"]==false) || $missing==false)
        {
            $str .= sprintf("<link href=\"%s\" rel=\"stylesheet\">\n", $src);
            self::$assets["cssSource"][$src]["active"] =true;
        }
        return $str;
    }

    public function QbaBitAssetsRenderScriptSource($missing=false)
    {
        $str = '';
        foreach ($this->parseOrderSources(self::$assets['jsSource']) as $src)
        if(($missing==true && self::$assets['jsSource'][$src]["active"]==false) || $missing==false)
        {
            $str .= sprintf("<script src=\"%s\" type=\"text/javascript\"></script>\n", $src);
            self::$assets["jsSource"][$src]["active"] =true;
        }
        self::$assets = null;
        return $str;
    }
    public function QbaBitAssetsRenderPostScriptSource($list,$control,$event)
    {
        $str = '';
        $srv = $_SERVER["SERVER_NAME"];
        $port =  $_SERVER["SERVER_PORT"];
        $path = "";
        if ($port==80)
            $path = "http://".$srv;
        //    $path = substr($_SERVER["HTTP_REFERER"],0,strpos($_SERVER["HTTP_REFERER"],$srv)+strlen($srv));
        $data = array("src"=>array(),"control"=>$control,"method"=>$event);
        foreach ($list as $src)
            $data["src"][] = $path.$src;
        $str .= sprintf("MKLib.scripts.processList(%s); ",json_encode($data) );

        return $str;
    }

}
