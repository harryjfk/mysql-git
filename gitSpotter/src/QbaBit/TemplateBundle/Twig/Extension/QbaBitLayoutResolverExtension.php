<?php

namespace QbaBit\TemplateBundle\Twig\Extension;

use QbaBit\TemplateBundle\Libs\Interfaces\QbaBitTemplateInterface;
use QbaBit\TemplateBundle\Services\QbTemplateService;

class QbaBitLayoutResolverExtension extends \Twig_Extension
{

    /**
     * @var \QbaBit\TemplateBundle\Services\QbTemplateService
    */
    private $template;

    /**
     * @param \QbaBit\TemplateBundle\Services\QbTemplateService $template
    */
    public function __construct(QbTemplateService $template)
    {
        $this->template = $template;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'qba_bit_layout_resolver';
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('QbaBitLayoutToExtend', array($this, 'QbaBitLayoutToExtend'))
        );
    }

    public function QbaBitLayoutToExtend(string $layout, string  $scope = "", QbaBitTemplateInterface $tpl = null)
    {
        if(!$tpl){
            $tpl = $this->template->getTemplateObject($scope);
        }

        return $this->template->buildTemplatePath($layout, "", $tpl ? $tpl->getAlias() : '');
    }
}
