<?php

namespace QbaBit\TemplateBundle\Form;

use Doctrine\ORM\EntityManager;
use QbaBit\CurrencyBundle\Entity\QbCurrency;
use QbaBit\FormsBundle\Form\Base\AccessBaseType;
use QbaBit\FormsBundle\Form\Types\Basic\FileControlType;
use QbaBit\FormsBundle\Form\Types\Basic\HtmlControlType;
use QbaBit\FormsBundle\Form\Types\Basic\iCheckType;
use QbaBit\FormsBundle\Form\Types\Basic\SearchType;
use QbaBit\FormsBundle\Form\Types\Collections\FilteredEntityType;
use QbaBit\FormsBundle\Form\Types\Crud\ListItemCrudType;
use QbaBit\FormsBundle\Form\Types\Images\ImageFileMultipleType;
use QbaBit\FormsBundle\Form\Types\Images\ImageFileType;
use QbaBit\FormsBundle\Form\Types\Images\ImageGalleryType;
use QbaBit\LanguageBundle\Entity\QbLanguage;

use QbaBit\TemplateBundle\Entity\QbTemplate;
use QbaBit\TemplateBundle\Entity\QbTemplateCategory;
use QbaBit\TemplateBundle\Entity\QbTemplateImages;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QbTemplateListItemType extends ListItemCrudType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
       parent::buildForm($builder,$options);
        $builder
            ->add('images', ImageGalleryType::class,array("label"=>false,'upload_web'=>$options["upload_web"],'primary'=>'defaultImg','field'=>$options["upload_web"]==null?'name':'id'));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('upload_web', null);
    }
}
