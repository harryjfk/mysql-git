<?php

namespace QbaBit\MainFrameBundle\Form;

use QbaBit\CurrencyBundle\Entity\QbCurrency;
use QbaBit\FormsBundle\Form\Base\AccessBaseType;
use QbaBit\FormsBundle\Form\Types\Basic\iCheckType;
use QbaBit\FormsBundle\Form\Types\Images\ImageFileType;
use QbaBit\LanguageBundle\Entity\QbLanguage;
use QbaBit\MainFrameBundle\Entity\QbTemplatesCategory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QbTemplateCategoryType extends AccessBaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['label'=>'name', 'translation_domain'=>'template_category'])
          ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'data_class' => QbTemplateCategory::class,
            "compound"=>true,

        ]);
    }
}
