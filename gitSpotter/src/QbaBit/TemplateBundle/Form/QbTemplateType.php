<?php

namespace QbaBit\TemplateBundle\Form;

use Doctrine\ORM\EntityManager;
use QbaBit\CurrencyBundle\Entity\QbCurrency;
use QbaBit\FormsBundle\Form\Base\AccessBaseType;
use QbaBit\FormsBundle\Form\Types\Basic\FileControlType;
use QbaBit\FormsBundle\Form\Types\Basic\HtmlControlType;
use QbaBit\FormsBundle\Form\Types\Basic\iCheckType;
use QbaBit\FormsBundle\Form\Types\Collections\FilteredEntityType;
use QbaBit\FormsBundle\Form\Types\Images\ImageFileMultipleType;
use QbaBit\FormsBundle\Form\Types\Images\ImageFileType;
use QbaBit\LanguageBundle\Entity\QbLanguage;

use QbaBit\TemplateBundle\Entity\QbTemplate;
use QbaBit\TemplateBundle\Entity\QbTemplateCategory;
use QbaBit\TemplateBundle\Entity\QbTemplateImages;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QbTemplateType extends AccessBaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['label'=>'name', 'translation_domain'=>'templates'])
            ->add('images', ImageFileMultipleType::class, array('search_callback' => function ($arrayCollection, $src) {
                /*foreach ($arrayCollection as $t)
                    if ($t->getImage() == $src->name)
                        return $t;*/
                return false;
            },
                'new_callback' => function (QbTemplateImages $image, $src, $generated_filename) {

                    if ($generated_filename != null)
                        $image->setName($generated_filename);
                    if ($src->default == true)
                        $image->getTemplate()->setImageDefault($image);

                }, 'retrieve_file_callback' => function ($object) {
                    return $object->getName();

                }, 'delete_callback' => function (QbTemplateImages $object) {

                    if ($object->getTemplate()->getImageDefault() == $object)
                        $object->getTemplate()->setImageDefault(null);

                }, 'default_callback' => function (QbTemplateImages $object, EntityManager $em) {
                    return $object->getTemplate()->getImageDefault() == $object;

                }, "canDoUrl"=>false,'label' => 'images',"translation_domain"=>"templates", 'attr' => array('class' => 'form-control'), 'required' => true))
            ->add('zip_file', FileControlType::class, array( 'label' => 'zip_file','translation_domain'=>'templates',"extension"=>array("zip"), 'attr' => array('class' => 'form-control'), 'required' => true))
            ->add('description', HtmlControlType::class, array('label'=>'description', 'translation_domain'=>'templates', 'required' => true, 'attr' => array('class' => '')))
            ->add('price', NumberType::class, array('label'=>'price', 'translation_domain'=>'templates', 'required' => true, 'attr' => array('class' => 'form-control')))
            ->add('category', FilteredEntityType::class, array('label'=>'category',"class"=>QbTemplateCategory::class, 'translation_domain'=>'templates', 'required' => true, 'attr' => array('class' => '')))

            /*  ->add('active', iCheckType::class, array('label'=>'active', 'translation_domain'=>'language', 'required' => false, 'attr' => array('class' => '')))
                ->add('shortCode', null, array( 'label' => 'shortCode',  'translation_domain'=>'language','attr' => array('class' => 'form-control'), 'required' => false))
              ->add('longCode', null, array( 'label' => 'longCode',  'translation_domain'=>'language','attr' => array('class' => 'form-control'), 'required' => false))
            */  ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'data_class' => QbTemplate::class,
            "compound"=>true,

        ]);
    }
}
