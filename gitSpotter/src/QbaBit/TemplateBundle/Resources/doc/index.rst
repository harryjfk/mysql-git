#############
Interfaces
#############

La entidad Template o las plantillas que se vayan a definir en el sistema tienen que implementar la interface
src/QbaBit/TemplateBundle/Libs/Interfaces/QbaBitTemplateInterface, define dos métodos:
 * getAlias() devuelve el nombre del directorio que contiene la plantilla
 * getScope() devuelve el scope de la plantilla front, backend, etc según se defina...

#############
Servicios
#############

Se han definido los siguientes servicios:

 * qbabit.template, corresponde a la clase src/QbaBit/TemplateBundle/Services/QbTemplateService, es el encargado
   de renderizar las plantillas y buscar la entidad plantilla en la base de datos según el scope, para mayor
   ver los comentarios de cada método en la clase.

 * qbabit.template.seo, src/QbaBit/TemplateBundle/Twig/Extension/QbaBitSeoExtension, se ha creado una extensión de twig
   para enviar títulos y metas a la plantilla y facilitar el trabajo básico para SEO. La plantilla base está configurada
   para renderizar los metas y el título según se definan. Se pueden ver más detalles del servicio en la clase. Al ser
   una extensión de tiwg sus métodos pueden ser utilizados directamente en las plantillas twig.

 * qbabit.template.assets, src/QbaBit/TemplateBundle/Twig/Extension/QbaBitAssetsExtension, es otra extensión de twig
   para agregar css y js sin que se repitan en la web, existen los métodos QbaBitAssetsRenderCssSources y QbaBitAssetsRenderScriptSource
   que renderizan las etiquetas js y css. Los métodos QbaBitAssetsAddCssSource y QbaBitAssetsAddScriptSource reciben la
   url de los css o js que serán renderizados, estas funciones tienen un segundo parámetro que es para añadir la url al final
   de la lista a renderizar.

 * qbabit.template.layout_resolver, src/QbaBit/TemplateBundle/Twig/Extension/QbaBitLayoutResolverExtension, es una exntesión
   de twig que se utiliza para determinar de que layout se debe heredar y el scope de la pantilla a utilizar

.. code-block:: tiwg
   :linenos:
    {# usando SEO #}
    <title>{% block title %}{{ QbaBitSeoTitle() }}{% endblock %}</title>
    {% block stylesheets %}{% endblock %}
    {{ QbaBitRenderMetas() | raw }}

    {# usando assets #}
    {{ QbaBitAssetsRenderCssSources() | raw }}

    {# renderizando en un bloque #}
    {% block javascripts %}{{ QbaBitAssetsRenderScriptSource() | raw }} {% endblock %}

    {# en este caso lo que está diciendo es busca una platilla con scope "backend" que tenga un layout.html.tiwg para hereda #}
    {# si no lo encuentra entonces herada de '@QbaBitTemplate/layout.html.twig' #}
    {% extends QbaBitLayoutToExtend('@QbaBitTemplate/layout.html.twig', 'backend') %}

    {# en este caso se agrega el objeto objectOfQbaBitTemplateInterface que es una instancia de QbaBitTemplateInterface #}
    {# y lo que quiere decir es lo siguiente: busca la plantilla  objectOfQbaBitTemplateInterface con un layout.html.twig #}
    {# y si no está disponible hereda de '@QbaBitTemplate/layout.html.twig' o sea al hacer esto de hace caso omiso de 'backend' #}
    {% extends QbaBitLayoutToExtend('@QbaBitTemplate/layout.html.twig', 'backend', objectOfQbaBitTemplateInterface) %}

#############
Estructura de las plantillas
#############

Todas las plantillas deben ir ubicadas en:
 src/QbaBit/TemplateBundle/Resources/  adentro de este directorio la estructura de la plantilla para redefinir
 secciones debería ser la siguiente:
 adminlte (código de plantilla y directorio que contiene la plantilla)
     bundles
         QbaBitTemplateBundle (indica que se va a hacer redefiniciones de vistas para este bundle)
             Default (directorio que contiene vistas a redefinir, tiene que llamarse igual que en el bundle QbaBitTemplateBundle)
                 index.html.twig (tiwg redefinido)
     layout.html.twig (plantilla de la que heredarán el resto de vistas cuando la plantilla adminlte esté activada)
     Services (permite registrar servicios en caso de que sea necesario utilizarlos en las plantillas)
         services.yml (contiene la configuración de los servicios de la plantilla)

 En la dirección src/QbaBit/TemplateBundle/Resources/public debe existir otro directorio con el mismo nombre de
 la plantilla o sea adminlte que contendrá los assets de la plantilla, se debe hacer de esta forma para aprovechar
 el comando assets:install de symfony. El instalador de plantillas debe ser el encargado de distribuir todos estos
 directorios.

#############
Dependency Injection
#############

En la extensión que trae por defecto el bundle TemplateBundle se ha creado la configuración
qba_bit_template.path_template_views, tiene por defecto la ruta al directorio views del bundele TemplateBundle
la cual debe ser utilizada para las instalaciones de las plantillas en caso necesario.