<?php
/**
 * Created by PhpStorm.
 * User: cronk
 * Date: 3/29/18
 * Time: 12:52 PM
 */

namespace QbaBit\TemplateBundle\Resources\views\adminlte\Classes;


use QbaBit\MenuBundle\Libs\Classes\QbaBitAbstractRenderMenuService;
use QbaBit\MenuBundle\Libs\Interfaces\QbaBitMenuInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AdminLteRenderItemService extends QbaBitAbstractRenderMenuService
{

    /**
     * @param \Symfony\Component\DependencyInjection\ContainerInterface
     */
    public function __construct(ContainerInterface $container)
    {
       parent::__construct($container);
    }

    /**
     * @param QbaBitMenuInterface $menu menu item to render
     * @return string return rendered submenu
     */
    public function render(QbaBitMenuInterface $menu)
    {
        if($menu->getConfiguration()->getRenderMode() == 'root') {
            return "<li>MiRendered " . $menu->getName(). "ROOT</li>";
        }
        else{
            if($menu->getConfiguration()->getRenderMode() == 'childs' and false)
            return "<li>MiRendered " . $menu->getName() . "No deveria verse</li>";
        }
    }
}