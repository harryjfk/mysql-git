<?php

namespace QbaBit\TemplateBundle\Controller;


use QbaBit\CoreBundle\Core\Controller\QbaBitBaseController;
use QbaBit\TemplateBundle\Entity\QbTemplate;

class DefaultController extends QbaBitBaseController
{
    public function indexAction()
    {
        $this->get('qbabit.template.seo')->QbaBitSeoTitle('Título seo para hacer una prueba');
        $this->get('qbabit.template.seo')->QbaBitKeyWords('keywords de prueba para ver como funciona...');
        $this->get('qbabit.template.assets')->QbaBitAssetsAddCssSource('https://www.xatem.com/bundles/front/css/media.css');

//        return $this->renderQbabit('@QbaBitTemplate/Default/index.html.twig', array(
//            'param'=>$this->getParameter('qba_bit_template.path_template_views'),
//            'hereda'=>'base.html.twig'
//        ));

        $otherTpl = new QbTemplate();
        $otherTpl->setAlias('othertpl')->setScope('backend');

        return $this->renderQbabitScope('@QbaBitTemplate/Default/index.html.twig', 'backend', array(
            'param'=>$this->getParameter('qba_bit_template.path_template_views'),
            'hereda'=>$otherTpl
        ));
    }
}
