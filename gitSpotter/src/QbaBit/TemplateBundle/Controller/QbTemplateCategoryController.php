<?php

namespace QbaBit\TemplateBundle\Controller;

use QbaBit\CoreBundle\Core\Controller\QbaBitCrudController;
use QbaBit\CurrencyBundle\Entity\QbCurrency;
use QbaBit\CurrencyBundle\Form\QbCurrencyType;
use QbaBit\CoreBundle\Core\Controller\QbaBitBaseController;
use QbaBit\LanguageBundle\Entity\QbLanguage;
use QbaBit\TemplateBundle\Entity\QbTemplateCategory;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use QbaBit\CoreBundle\Core\Traits\FormErrorMessagesTrait;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/admec/templates_category")
 */
class QbTemplateCategoryController extends QbaBitCrudController
{
    public function getSingleTableName()
    {

        return "template_category";
    }
    public function getObjectAsAutorizathion($type)
    {
        $t = $this->getObjectAsArrayStr();
        $r = "ROLE_" . strtoupper(str_replace("Qb","",$t[3])) . "_" . strtoupper($type);

        return $r;
    }
    protected function getCurrentObject()
    {
        return QbTemplateCategory::class;
    }

    protected function getSearchForm()
    {
        $search = parent::getSearchForm();
        $search
            ->add('name', TextType::class, array("required" => false, 'label' => "name", "translation_domain" => "language", 'attr' => array('class' => 'form-control')));
        return $search;
    }

    /**
     * @Route("/", name="qb_templatescategory_index")
     */
    public function indexAction(Request $request): Response
    {

        return parent::indexAction($request);
    }

    /**
     * @Route("/new", name="qb_templatescategory_new", methods="GET|POST")
     */
    public function newAction(Request $request): Response
    {
        return parent::newAction($request);

    }

    /**
     * @Route("/{id}", name="qb_templatescategory_show", methods="GET")
     */
    public function showAction($id): Response
    {
        return parent::showAction($id);
    }

    /**
     * @Route("/{id}/edit", name="qb_templatescategory_edit", methods="GET|POST")
     */
    public function editAction(Request $request, $id): Response
    {
        return parent::editAction($request, $id);
    }

    /**
     * @Route("/{id}/delete", name="qb_templatescategory_delete", methods="DELETE")
     */
    public function deleteAction($id): Response
    {
        return parent::deleteAction($id);
    }
    /**
     * @Route("/deleteList", name="qb_templatescategory_delete_list", methods="DELETE")
     */
    public function deleteListAction(Request $request): Response
    {
        return parent::deleteListAction($request);
    }



}
