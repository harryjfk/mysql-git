<?php

namespace QbaBit\TemplateBundle\Controller;

use QbaBit\CoreBundle\Core\Classes\Reflection;
use QbaBit\CoreBundle\Core\Classes\RestBinder;
use QbaBit\CoreBundle\Core\Controller\QbaBitCrudController;
use GuzzleHttp\Client;
use QbaBit\CurrencyBundle\Entity\QbCurrency;
use QbaBit\CurrencyBundle\Form\QbCurrencyType;
use QbaBit\CoreBundle\Core\Controller\QbaBitBaseController;
use QbaBit\LanguageBundle\Entity\QbLanguage;

use QbaBit\TemplateBundle\Entity\QbTemplate;
use QbaBit\TemplateBundle\Entity\QbTemplateCategory;
use QbaBit\TemplateBundle\Entity\QbTemplateImages;
use QbaBit\TemplateBundle\Form\QbTemplateListItemType;
use QbaBit\TemplateBundle\Libs\Embeddable\QbTemplateSelectedConfiguration;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use QbaBit\CoreBundle\Core\Traits\FormErrorMessagesTrait;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/admec/templates")
 */
class QbTemplateController extends QbaBitCrudController
{



    protected function additionalRenderParams($crud_mode = "list")
    {
        $t = parent::additionalRenderParams($crud_mode); // TODO: Change the autogenerated stub
        $t["showListType"] = "view";
        return $t;
    }

    protected function getCurrentObject()
    {
        return QbTemplate::class;
    }

    protected function getSearchFormOptions()
    {
        return array("itemType" => QbTemplateListItemType::class);
    }

    protected function getSearchForm()
    {
        $search = parent::getSearchForm();
        $search
            ->add('name', TextType::class, array("required" => false, 'label' => "name", "translation_domain" => "language", 'attr' => array('class' => 'form-control')));
        return $search;
    }

    /**
     * @Route("/", name="qb_template_index")
     */
    public function indexAction(Request $request): Response
    {

        return parent::indexAction($request);
    }


    /**
     * @Route("/new", name="qb_template_new", methods="GET|POST")
     */
    public function newAction(Request $request): Response
    {
        return parent::newAction($request);

    }

    /**
     * @Route("/{id}/show", name="qb_template_show", )
     */
    public function showAction($id): Response
    {
        if (!$this->isGranted($this->getUrlRoles()["show"])) {
            throw $this->createAccessDeniedException();
        }
        $template = $this->getRepository()->find($id);
        if ($template == null)
            throw $this->createNotFoundException();
        $view_images = $this->createForm("QbaBit\\TemplateBundle\\Form\\QbTemplateViewType", $template, array('action' => $this->generateUrl($this->getListUrl())));

        return $this->renderQbabitScope('@QbaBitTemplate/QbTemplate/show.html.twig', 'backend', ["images" => $view_images->createView(), "error" => false, "data" => $template]);


    }

    /**
     * @Route("/{id}/edit", name="qb_template_edit", methods="GET|POST")
     */
    public function editAction(Request $request, $id): Response
    {
        $t = new QbTemplateSelectedConfiguration();
        $t->setColor("blue");
        $template = $this->get("doctrine.orm.default_entity_manager")->getRepository("QbaBitMainFrameBundle:QbMnTemplate")->findOneBy(array("alias" => "adminlte"));

        $t->setTemplate($template);
        $editForm = $this->createForm("QbaBit\FormsBundle\Form\Types\Configurations\ConfigurationCollectionType", $t);
        $entity = $this->getRepository()->find($id);
        $url = $this->generateUrl($this->getEditUrl(), array("id" => $id, 'slugSelected' => $request->get('slugSelected')));
        // $editForm = $this->getForm($entity, $url);
        $result = $this->handleForm($request, $entity, $editForm);
        if ($result == false) {


            return $this->renderQbabitScope($this->getEditRender(), 'backend', [
                'entity' => $entity,
                "isNew" => false,
                'form' => $editForm->createView()]);
        }

        return $result;
        //return parent::editAction($request, $id);
    }

    /**
     * @Route("/{id}/delete", name="qb_template_delete", methods="DELETE")
     */
    public function deleteAction($id): Response
    {
        return parent::deleteAction($id);
    }

    /**
     * @Route("/deleteList", name="qb_template_delete_list", methods="DELETE")
     */
    public function deleteListAction(Request $request): Response
    {
        return parent::deleteListAction($request);
    }


    protected function getSearchFormInstall($fromTemplate = false)
    {
        $url = $this->generateUrl("qb_template_install");
        if ($fromTemplate)
            $url .= "?fromTemplate=1";
        $search = $this->createForm("QbaBit\\FormsBundle\\Form\\Types\\Basic\\SearchType", null, array("action" => $url));

        $search
            ->add('name', TextType::class, array("required" => false, 'label' => "name", "translation_domain" => "language", 'attr' => array('class' => 'form-control')));

        return $search;
    }

    protected function getFilterConfigurationsInstall(FormInterface $form, $asArray = true)
    {
        $filter = $form->getData();
        if ($filter == null)
            $filter = array();
        $filter = array_merge($filter, $this->additionalFilterParams());
        if ($asArray)
            return $filter;
        $r = "";
        foreach ($filter as $k => $v)
            if ($k != "listData" && $k != "submit" && "fromTemplate")
                $r .= $k . '=' . $v . '&';


        return $r;
    }

    protected function getHttpResponse($url, $file = null)
    {
        $client = new Client();
        $error = false;
        $errorMsg = "";
        try {
            if ($file != null) {
                $result = $client->request("GET",
                    $url, ['sink' => $file]

                );
                $result = new RestBinder($result);
            } else {
                $result = $client->request("GET",
                    $url

                );
                $result = new RestBinder($result);
            }

        } catch (\Exception $e) {
            $error = true;
            $errorMsg = $e->getMessage();

        }
        return array("error" => $error, "errorMsg" => $errorMsg, "result" => isset($result) ? $result : null);
    }

    /**
     * @Route("/install", name="qb_template_install")
     */
    public function installAction(Request $request): Response
    {
        if (!$this->isGranted($this->getUrlRoles()["install"])) {
            throw $this->createAccessDeniedException();
        }
        $fromTemplate = $request->get("fromTemplate");
        $searchForm = $this->getSearchFormInstall($fromTemplate != null);
        $searchForm->handleRequest($request);
        $params = $this->getFilterConfigurationsInstall($searchForm, false);


        $trans = $this->container->get("translator");


        $returnButton = $fromTemplate == null ? null : $this->createForm("QbaBit\FormsBundle\Form\Types\Basic\ButtonType", null, array("caption" => $trans->trans("back_to_list", array(), "qbabitcore"), "buttonType" => "primary", "action" => $this->generateUrl($this->getListUrl())))->createView();


        $url = $this->getParameter("qbabit_rest_service_template") . "/list?" . ($params);

        $res = $this->getHttpResponse($url);

        if ($res["error"] == false) {
            $paginator = $this->get('knp_paginator');


            $data = $res["result"]->getData();

            foreach ($data["data"] as $k => $template_src) {


                $template = $this->getRepository()->findOneBy(array("alias" => $template_src["alias"]));
                $installed = ($template != null);
                $data["data"][$k]["installed"] = $installed;
            }
            $res["result"]->setData($data);
            $data = $paginator->paginate(
                $res["result"],
                $request->query->getInt('page', 1),
                $this->getParameter('qbabit_pagination_total_records')
            );
            $params = $this->getFilterConfigurationsInstall($searchForm);
            $params["listData"] = array("all" => false, "rows" => $data);
            $searchForm = $this->getSearchFormInstall($fromTemplate != null);
            $searchForm->setData($params);
            return $this->renderQbabitScope('@QbaBitTemplate/QbTemplate/index_install.html.twig', 'backend', ["returnButton" => $returnButton, "searchForm" => $searchForm->createView(), 'data' => $data]);

        } else {
            $searchForm = $this->getSearchFormInstall();
            return $this->renderQbabitScope('@QbaBitTemplate/QbTemplate/index_install.html.twig', 'backend', ["returnButton" => $returnButton, "searchForm" => $searchForm->createView(), "error" => $res["error"], "errorMsg" => $res["errorMsg"]]);

        }


    }

    /**
     * @Route("/install_show/{id}", name="qb_template_install_show")
     */
    public function showInstallAction(Request $request, $id): Response
    {
        if (!$this->isGranted($this->getUrlRoles()["install_show"])) {
            throw $this->createAccessDeniedException();
        }
        $url = $this->getParameter("qbabit_rest_service_template") . "/" . $id;
        $res = $this->getHttpResponse($url);

        if ($res["error"] == false) {
            $fromTemplate = $request->get("fromTemplate");
            $data = $res["result"]->getData();
            $installed = $this->getRepository()->findOneBy(array("alias" => $data["alias"]));
            $form_url = $this->generateUrl($this->getInstallUrl());
            if ($form_url != null)
                $form_url .= "?fromTemplate=1";
            $view_images = $this->createForm("QbaBit\\TemplateBundle\\Form\\QbTemplateViewType", $data, array("upload_web" => $this->getParameter("qbabit_rest_service_template") . "/image/", 'action' => $form_url));


            return $this->renderQbabitScope('@QbaBitTemplate/QbTemplate/show_install.html.twig', 'backend', ["installed" => $installed, "images" => $view_images->createView(), "error" => $res["error"], "data" => $data]);

        } else {
            return $this->renderQbabitScope('@QbaBitTemplate/QbTemplate/show_install.html.twig', 'backend', ["installed" => false, "error" => $res["error"], "errorMsg" => $res["errorMsg"]]);
        }
    }

    protected function install($id)
    {
        $url = $this->getParameter("qbabit_rest_service_template") . "/" . $id;
        $data = $this->getHttpResponse($url);
        $trans = $this->get("translator");
        if ($data["error"] == false) {

            try {
                $data = $data["result"]->getData();
                $template = $this->getRepository()->findOneBy(array("alias" => $data["alias"]));
                if ($template != null)

                    return (["error" => true, "errorMsg" => $trans->trans("js_error_instalar_already", array(), "template")]);

                $url_file = $this->getParameter("qbabit_rest_service_template") . "/download/" . $id;
                $folder = $this->getParameter("qbabit_path_uploads_temp");
                $f = $folder . $data["filename"];
                $data_file = $this->getHttpResponse($url_file, $f);
                $file_info = (new \SplFileInfo($data["filename"]));
                $t = str_replace($file_info->getExtension(), "", $file_info->getFilename());
                if ($t[strlen($t) - 1] == ".")
                    $t = substr($t, 0, strlen($t) - 1);
                $zip = new \ZipArchive();
                if ($zip->open($f, \ZipArchive::CREATE) !== TRUE) {
                    exit("cannot open <$f>\n");
                }


                $dir = $folder . "/" . $t;

                if (!is_dir($dir))
                    mkdir($dir);


                $zip->extractTo($dir);

                $zip->close();


                $template_name = $data["alias"];


                $asset_folder = $this->getParameter("kernel.root_dir") . "/QbaBit/TemplateBundle/Resources/public/" . $template_name;
                $layout_folder = $this->getParameter("kernel.root_dir") . "/QbaBit/TemplateBundle/Resources/views/" . $template_name;
                if (!is_dir($asset_folder))
                    mkdir($asset_folder);
                if (!is_dir($layout_folder))
                    mkdir($layout_folder);
                $this->get("qbabit.core.file_utils")->recurse_copy($folder . "/" . $t . "/assets", $asset_folder);
                $this->get("qbabit.core.file_utils")->recurse_copy($folder . "/" . $t . "/layout", $layout_folder);

                exec("php " . $this->getParameter("kernel.project_dir") . "/bin/console assets:install");
                exec("php " . $this->getParameter("kernel.project_dir") . "/bin/console cache:clear");

                $this->get("qbabit.core.file_utils")->removeDir($folder . "/" . $t);
                //   unlink($f);

                $em = $this->get("doctrine.orm.entity_manager");

                //Buscar las categorias
                $category = $em->getRepository(QbTemplateCategory::class)->findOneBy(array("name" => $data["category"]["name"]));

                if ($category == null) {
                    $category = new QbTemplateCategory();
                    $category->setName($data["category"]["name"]);
                    $em->persist($category);
                    $em->flush();
                }


                $template = $em->getRepository(QbTemplate::class)->findOneBy(array("alias" => $data["alias"]));

                if ($template == null)

                    $template = new QbTemplate();
                $template->setName($data["name"]);
                $template->setAlias($data["alias"]);
                $template->setDescription($data["description"]);
                $template->setCategory($category);
                $template->setScope($data["scope"]);
                $template->setImageDefault(null);
                $template->setPrice($data["price"]);
                $template->setFilename($data["filename"]);
                $default_img = null;
                foreach ($data["images"] as $image_src) {

                    $image = $em->getRepository(QbTemplateImages::class)->findOneBy(array("name" => $image_src["name"]));

                    if ($image == null) {
                        $image = new QbTemplateImages();
                        $image->setTemplate($template);
                        $image->setName($image_src["name"]);
                        $url_file = $this->getParameter("qbabit_rest_service_template") . "/image/" . $image_src["id"];
                        $data_file = $this->getHttpResponse($url_file, $this->getParameter("qbabit_path_uploads_temp") . $image_src["name"]);

                        $template->addImage($image);
                    }
                    if (is_array($data["image_default"]))
                        if ($data["image_default"]["id"] == $image_src["id"])
                            $default_img = $image;
                }
                $em->persist($template);
                $em->flush();
                $template->setImageDefault($default_img);
                $em->persist($template);
                $em->flush();
            } catch (\Exception $exception) {

                $error = true;
                $errorMsg = $exception->getMessage();
                return (["error" => $error, "errorMsg" => $trans->trans("install_error", array("{error}" => $errorMsg), "template")]);

            }


            return ["error" => false, "msg" => $trans->trans("install_success", array("{template}" => $data["name"]), "template")];


        } else {
            return ["error" => $data["error"], "errorMsg" => $data["errorMsg"]];
        }
    }

    /**
     * @Route("/install_do/{id}", name="qb_template_install_do")
     */
    public function doInstallAction($id): Response
    {
        if (!$this->isGranted($this->getUrlRoles()["install_do"])) {
            throw $this->createAccessDeniedException();
        }
        return new JsonResponse($this->install($id));

    }

    protected function getUninstallUrl()
    {
        return $this->getObjectAsRouteURL() . "_uninstall_do";
    }

    protected function getUninstallAllUrl()
    {
        return $this->getObjectAsRouteURL() . "_uninstall_do_all";
    }

    protected function getInstallUrl()
    {
        return $this->getObjectAsRouteURL() . "_install";
    }

    protected function getInstallShowUrl()
    {
        return $this->getObjectAsRouteURL() . "_install_show";
    }

    protected function getInstallDoUrl()
    {
        return $this->getObjectAsRouteURL() . "_install_do";
    }

    protected function getInstallDoAllUrl()
    {
        return $this->getObjectAsRouteURL() . "_install_do_all";
    }

    /**
     * @Route("/install_do_all/}", name="qb_template_install_do_all")
     */
    public function doInstallAllAction(Request $request): Response
    {
        $json = array('status' => 1, 'ids' => array(), 'error' => array(), 'stringError' => '');

        if (!$this->isGranted($this->getUrlRoles()["install_do_all"])) {
            throw $this->createAccessDeniedException();
        }


        $listIds = $request->get('qba_bit_search_form', array());
        if (array_key_exists("listData", $listIds))
            $listIds = $listIds["listData"]["rows"];
        foreach ($listIds as $row => $v)
            if (array_key_exists("checked", $v)) {

                try {
                    $this->install($v["id"]);
                    $json['ids'][] = $v["id"];
                } catch (\Exception $e) {
                }

            }

        if (!count($json['ids'])) {
            $json['status'] = 0;
        }

        return new JsonResponse($json);

    }

    protected function uninstall($id)
    {
        $template = $this->getRepository()->find($id);
        $template_base = $template->getName();
        $trans = $this->get("translator");
        $error = false;
        $errorMsg = "";

        $front = $this->get("qbabit.template")->getTemplateObject("frontend");
        $back = $this->get("qbabit.template")->getTemplateObject("backend");
        if ($front->getAlias() == $template->getAlias() || $back->getAlias() == $template->getAlias()) {
            $error = true;
            $errorMsg = $trans->trans("js_template_in_use", array(), "template");
        } else {
            try {
                /*
      *  $template_name=$template->getAlias();
     $asset_folder = $this->getParameter("kernel.root_dir") . "/QbaBit/TemplateBundle/Resources/public/" . $template_name;
     $layout_folder = $this->getParameter("kernel.root_dir") . "/QbaBit/TemplateBundle/Resources/views/" . $template_name;

     $this->get("qbabit.core.file_utils")->removeDir($asset_folder);
     $this->get("qbabit.core.file_utils")->removeDir($layout_folder);
     exec("php ".$this->getParameter("kernel.project_dir")."/bin/console assets:install") ;
     exec("php ".$this->getParameter("kernel.project_dir")."/bin/console cache:clear") ;
     $em = $this->get("doctrine.orm.default_entity_manager");
     $em->remove($template);

     */
                return ["error" => false, "msg" => $trans->trans("uninstall_success", array("{template}" => $template_base), "template")];

            } catch (\Exception $exception) {

                $error = true;
                $errorMsg = $exception->getMessage();
                return (["error" => $error, "errorMsg" => $trans->trans("uninstall_error", array("{error}" => $errorMsg), "template")]);

            }
        }
        return array("error" => $error, "errorMsg" => $errorMsg);
    }


    /**
     * @Route("/uninstall_do/{id}", name="qb_template_uninstall_do")
     */
    public function doUninstallAction($id): Response
    {
        if (!$this->isGranted($this->getUrlRoles()["uninstall_do"]))
            throw $this->createAccessDeniedException();

        return new JsonResponse($this->uninstall($id));

    }

    /**
     * @Route("/uninstall_do_all/}", name="qb_template_uninstall_do_all")
     */
    public function doUninstallAllAction(Request $request): Response
    {
        $json = array('status' => 1, 'ids' => array(), 'error' => array(), 'stringError' => '');

        if (!$this->isGranted($this->getUrlRoles()["uninstall_do_all"])) {
            throw $this->createAccessDeniedException();
        }


        $listIds = $request->get('qba_bit_search_form', array());
        if (array_key_exists("listData", $listIds))
            $listIds = $listIds["listData"]["rows"];
        foreach ($listIds as $row => $v)
            if (array_key_exists("checked", $v)) {

                try {
                    $t = $this->uninstall($v["id"]);
                    if ($t["error"] == false)
                        $json['ids'][] = $v["id"];
                } catch (\Exception $e) {
                }

            }

        if (!count($json['ids'])) {
            $json['status'] = 0;
        }

        return new JsonResponse($json);

    }

    public function getUrlRoutes()
    {
        $base = array();
        $base["install"] = $this->getInstallUrl();
        $base["view"] = $this->getListUrl();
        $base["install_show"] = $this->getInstallShowUrl();
        $base["install_do"] = $this->getInstallDoUrl();
        $base["uninstall_do"] = $this->getUninstallUrl();
        $base["uninstall_do_all"] = $this->getUninstallAllUrl();
        $base["show"] = $this->getShowUrl();
        $base["install_do_all"] = $this->getInstallDoAllUrl();
        return $base;
    }

    public function getUrlRoles()
    {
        $base = array();
        $base["install"] = $this->getObjectAsAutorizathion("install");
        $base["view"] = $this->getObjectAsAutorizathion("view");
        $base["show"] = $this->getObjectAsAutorizathion("show");
        $base["uninstall_do"] = $this->getObjectAsAutorizathion("uninstall_do");
        $base["uninstall_do_all"] = $this->getObjectAsAutorizathion("uninstall_do_all");
        $base["install_show"] = $this->getObjectAsAutorizathion("install_show");
        $base["install_do"] = $this->getObjectAsAutorizathion("install_do");
        $base["install_do_all"] = $this->getObjectAsAutorizathion("install_do_all");

        return $base;
    }
    /**
     * @Route("/templateConfig/", name="qb_template_template_configs", )
     */
    public function templateConfigAction(Request $request): Response
    {
        if (!$this->isGranted($this->getUrlRoles()["show"])) {
            throw $this->createAccessDeniedException();
        }
        $template = $this->getRepository()->find($request->get("id"));
        if ($template == null)
            throw $this->createNotFoundException();

        $data = new QbTemplateSelectedConfiguration();
        $data->setTemplate($template);

        $inheterance =  Reflection::getConfigurationInheterance(QbTemplateSelectedConfiguration::class,$data,$request->get("asIndexed",true)=="true");

        foreach ($inheterance as $k=>$v)
            foreach ($v as $k1=>$v1)
                $inheterance[$k][$k1] = str_replace("template.metadata.","",$v1);

        return new JsonResponse(json_encode($inheterance));

    }

}
