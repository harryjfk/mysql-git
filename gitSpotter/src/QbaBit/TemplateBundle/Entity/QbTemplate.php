<?php
/**
 * Created by PhpStorm.
 * User: cronk
 * Date: 3/25/18
 * Time: 5:52 PM
 */

namespace QbaBit\TemplateBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use QbaBit\CoreBundle\Core\Classes\ArrayGetter;
use QbaBit\CoreBundle\Core\Traits\FileUploadable;
use QbaBit\CoreBundle\Core\Traits\Identificable;
use QbaBit\CoreBundle\Core\Traits\Nameable;
use QbaBit\TemplateBundle\Libs\Embeddable\QbTemplateConfiguration;
use QbaBit\TemplateBundle\Libs\Embeddable\QbTemplateSelectedConfiguration;
use QbaBit\TemplateBundle\Libs\Interfaces\QbaBitTemplateInterface;
use QbaBit\TemplateBundle\Entity\QbTemplateCategory;

/**
 * QbTemplate
 *
 * @ORM\Table(name="qb_templates")
 * @ORM\Entity(repositoryClass="QbaBit\TemplateBundle\Repository\QbTemplateRepository")
 */
class QbTemplate extends ArrayGetter implements QbaBitTemplateInterface
{

    use Identificable,Nameable,FileUploadable ;
    /**
     * @var string
     *
     * @ORM\Column(name="alias", type="string", nullable=false)
     */
    private $alias = 'adminlte';

    /**
     * @var string
     *
     * @ORM\Column(name="scope", type="string", nullable=false)
     */
    private $scope = 'backend';

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     * @return QbTemplate
     */
    public function setAlias(string $alias): QbTemplate
    {
        $this->alias = $alias;
        return $this;
    }

    /**
     * @return string
     */
    public function getScope(): string
    {
        return $this->scope;
    }

    /**
     * @param string $scope
     * @return QbTemplate
     */
    public function setScope(string $scope): QbTemplate
    {
        $this->scope = $scope;
        return $this;
    }

    /**
     * @ORM\ManyToOne(targetEntity="QbaBit\TemplateBundle\Entity\QbTemplateCategory", inversedBy="templates", cascade={"all"}, fetch="EAGER")
     */
    private $category;
    /**
     * @return QbTemplateCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param QbTemplateCategory $category
     * @return QbTemplate
     */
    public function setCategory(QbTemplateCategory $category)
    {
        $this->category = $category;
        return $this;
    }
    /**
     * @ORM\OneToOne(targetEntity="QbaBit\TemplateBundle\Entity\QbTemplateImages")
     * @ORM\JoinColumn(name="image_default", referencedColumnName="id")
     */
    protected $imageDefault;


    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", nullable=false)
     */
    protected $price;

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     * @return QbTemplate
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    protected $description;

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return QbTemplate
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @ORM\OneToMany(targetEntity="QbaBit\TemplateBundle\Entity\QbTemplateImages", mappedBy="template", cascade={"persist", "remove", "merge"}, orphanRemoval=true)
     */
    protected $images;



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add image.
     *
     * @param \QbaBit\TemplateBundle\Entity\QbTemplateImages $image
     *
     * @return QbTemplate
     */
    public function addImage(\QbaBit\TemplateBundle\Entity\QbTemplateImages $image)
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * Remove image.
     *
     * @param \QbaBit\TemplateBundle\Entity\QbTemplateImages $image
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeImage(\QbaBit\TemplateBundle\Entity\QbTemplateImages $image)
    {
        return $this->images->removeElement($image);
    }

    /**
     * Get images.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set imageDefault.
     *
     * @param \QbaBit\TemplateBundle\Entity\QbTemplateImages|null $imageDefault
     *
     * @return QbTemplate
     */
    public function setImageDefault(\QbaBit\TemplateBundle\Entity\QbTemplateImages $imageDefault = null)
    {
        $this->imageDefault = $imageDefault;

        return $this;
    }

    /**
     * Get imageDefault.
     *
     * @return \QbaBit\TemplateBundle\Entity\QbTemplateImages|null
     */
    public function getImageDefault()
    {
        return $this->imageDefault;
    }


    public function getFileVar()
    {
        return array('zip' => $this->getFilename());
    }
    public function setFileVar($name, $value)
    {
        $this->setFilename($value);

    }
    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255, nullable=false)
     */
    protected $filename;

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     * @return QbTemplate
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="metadata", type="text")
     */
    private $metadata;
    /**
     * Set metadata.
     *
     * @param \QbaBit\TemplateBundle\Libs\Embeddable\QbTemplateConfiguration $metadata
     *
     * @return QbTemplate
     */
    public function setMetadata(QbTemplateConfiguration $metadata)
    {

        $this->metadata = \JMS\Serializer\SerializerBuilder::create()->build()->serialize($metadata, 'json');

        return $this;
    }

    /**
     * Get metadata.
     *
     * @return \QbaBit\TemplateBundle\Libs\Embeddable\QbTemplateConfiguration
     */
    public function getMetadata()
    {
        return \JMS\Serializer\SerializerBuilder::create()->build()->deserialize($this->metadata, 'QbaBit\TemplateBundle\Libs\Embeddable\QbTemplateConfiguration', 'json');
    }

    /**
     * @var QbTemplateSelectedConfiguration
     */
    private $selectedConfiguration;

    /**
     * @return QbTemplateSelectedConfiguration
     */
    public function getSelectedConfiguration()
    {
        return $this->selectedConfiguration;
    }

    /**
     * @param QbTemplateSelectedConfiguration $selectedConfiguration
     * @return QbTemplate
     */
    public function setSelectedConfiguration($selectedConfiguration)
    {
        $this->selectedConfiguration = $selectedConfiguration;
        return $this;
    }

}