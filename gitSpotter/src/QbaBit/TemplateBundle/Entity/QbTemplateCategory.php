<?php

namespace QbaBit\TemplateBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use QbaBit\CoreBundle\Core\Classes\ArrayGetter;
use QbaBit\CoreBundle\Core\Traits\Enableable;
use QbaBit\CoreBundle\Core\Traits\FileUploadable;
use QbaBit\CoreBundle\Core\Traits\Identificable;
use QbaBit\CoreBundle\Core\Traits\Imageneable;
use QbaBit\CoreBundle\Core\Traits\Nameable;

/**
 * QbTemplateCategory
 *
 * @ORM\Table(name="qb_templates_category")
 * @ORM\Entity(repositoryClass="QbaBit\TemplateBundle\Repository\QbTemplateCategoryRepository")
 */
class QbTemplateCategory extends ArrayGetter
{

  use Identificable,Nameable;

    /**
     * @ORM\OneToMany(targetEntity="QbaBit\TemplateBundle\Entity\QbTemplate", mappedBy="category", cascade={"persist", "remove", "merge"}, orphanRemoval=true)
     */
    protected $templates;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->templates = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add template.
     *
     * @param \QbaBit\TemplateBundle\Entity\QbTemplate $template
     *
     * @return QbTemplateCategory
     */
    public function addTemplate(\QbaBit\TemplateBundle\Entity\QbTemplate $template)
    {
        $this->templates[] = $template;

        return $this;
    }

    /**
     * Remove template.
     *
     * @param \QbaBit\TemplateBundle\Entity\QbTemplate $template
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeTemplate(\QbaBit\TemplateBundle\Entity\QbTemplate $template)
    {
        return $this->templates->removeElement($template);
    }

    /**
     * Get templates.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTemplates()
    {
        return $this->templates;
    }
}
