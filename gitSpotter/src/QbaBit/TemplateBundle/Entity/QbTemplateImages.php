<?php

namespace QbaBit\TemplateBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use QbaBit\CoreBundle\Core\Classes\ArrayGetter;
use QbaBit\CoreBundle\Core\Traits\Enableable;
use QbaBit\CoreBundle\Core\Traits\FileUploadable;
use QbaBit\CoreBundle\Core\Traits\Identificable;
use QbaBit\CoreBundle\Core\Traits\Imageneable;
use QbaBit\CoreBundle\Core\Traits\Nameable;

/**
 * QbTemplateCategory
 *
 * @ORM\Table(name="qb_templates_images")
 * @ORM\Entity(repositoryClass="QbaBit\TemplateBundle\Repository\QbTemplateImagesRepository")
 */
class QbTemplateImages extends ArrayGetter
{

  use Identificable,Nameable;

    /**
     * @ORM\ManyToOne(targetEntity="QbaBit\TemplateBundle\Entity\QbTemplate",inversedBy="images", cascade={"all"}, fetch="EAGER")
     */
    private $template;

    /**
     * @return QbTemplate
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param QbTemplate $template
     * @return QbTemplateImages
     */
    public function setTemplate(QbTemplate $template)
    {
        $this->template = $template;
        return $this;
    }

}
