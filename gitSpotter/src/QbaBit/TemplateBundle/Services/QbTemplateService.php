<?php
/**
 * Created by PhpStorm.
 * User: cronk
 * Date: 3/25/18
 * Time: 11:55 AM
 */

namespace QbaBit\TemplateBundle\Services;


use Doctrine\ORM\EntityManagerInterface;
use QbaBit\TemplateBundle\Entity\QbTemplate;
use QbaBit\TemplateBundle\Libs\Embeddable\QbTemplateSelectedConfiguration;
use QbaBit\TemplateBundle\Libs\Interfaces\QbaBitTemplateInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Templating\EngineInterface;

class QbTemplateService
{

    /**
     * @var \Symfony\Bundle\TwigBundle\TwigEngine
     */
    protected $twig;

    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    protected $em;

    /**
     * @param \Symfony\Component\Templating\EngineInterface
     *
     */
    public function __construct(EngineInterface $twigEngine, EntityManagerInterface $em)
    {
        $this->twig = $twigEngine;
        $this->em = $em;
    }

    /**
     * @return array ['bundle', 'view', 'template']
     * @throws \Exception
     */
    private function transformTemplateRoute(string $pathTemplate)
    {
        $result = ['bundle' => '', 'view' => '', 'template' => ''];
        $parts = [];
        //@ format
        if (preg_match("/^@.*/", $pathTemplate)) {
            $cleanPath = trim(str_replace('@', '', $pathTemplate));
            $parts = explode('/', $cleanPath);
        }

        //:format
        if (preg_match("/\:/", $pathTemplate)) {
            $parts = explode(':', $pathTemplate);
        }

        if (!count($parts)) {
            throw new \Exception("Unrecognize template format", 400);
        }

        $result['bundle'] = (!preg_match("/Bundle$/", $parts[0])) ? sprintf('%sBundle', $parts[0]) : $parts[0];
        $result['view'] = array_key_exists(2, $parts) ? $parts[1] : '';
        $result['template'] = array_key_exists(2, $parts) ? $parts[2] : $parts[1];
        return $result;


    }

    public function buildTemplatePath($view, $overrideView = '', $tplName = '')
    {
        if (!$tplName and !$overrideView) {
            return $view;
        }

        $routesToSearch = [
            'withAllAndOverrideView' => '@QbaBitTemplate/%s/bundles/%s',
            'withOverrideViewInRootBundle' => '@%s/%s/%s',
            'withRootViewInTemplate' => '@QbaBitTemplate/%s/bundles/%s',
            'checkIfIsLayout' => '@QbaBitTemplate/%s/%s',
        ];

        $path = '';
        foreach ($routesToSearch as $k => $parser) {
            $parts = $this->transformTemplateRoute($view);
            $tempPath = '';
            switch ($k) {
                case 'withAllAndOverrideView':
                    {
                        if ($overrideView and $tplName) {
                            $parts['template'] = $overrideView;
                            $tempPath = sprintf($parser, $tplName, implode('/', $parts));
                        }
                        break;
                    }

                case 'withOverrideViewInRootBundle':
                    {
                        if ($overrideView) {
                            $parts['template'] = $overrideView;
                            $parts['bundle'] = str_replace('Bundle', '', $parts['bundle']);
                            $tempPath = sprintf($parser, $parts['bundle'], $parts['view'], $parts['template']);
                        }
                        break;
                    }
                case 'withRootViewInTemplate':
                    {
                        if ($tplName) {
                            $tempPath = sprintf($parser, $tplName, implode('/', $parts));
                        }
                        break;
                    }

                case 'checkIfIsLayout':
                    {
                        if ($tplName) {
                           $tempPath = sprintf($parser, $tplName, $parts['template']);
                        }
                        break;
                    }
            }


            if ($tempPath and $this->twig->exists($tempPath)) {
                $path = $tempPath;
                break;
            }
        }


        if ($path) {
            return str_replace('//', '/', $path);
        }

        return $view;
    }

    /**
     * @param string $view path to twig view
     * @param array $params view parameters
     * @param string $scope template scope
     * @param string $overrideVew name of twig view to override, the $overrideVew must be in the same directory that $view
     * @return string
     * @throws \Twig\Error\Error
     */
    public function renderTemplateScope($view, array $params = [], string $scope = '', string $overrideVew = '')
    {
        $tpl = $this->getTemplateObject($scope);
        return $this->twig->render($this->buildTemplatePath($view, $overrideVew, $tpl->getAlias()), array_merge($params,array("template"=>$tpl)) );
    }

    /**
     * @param string $view path to twig view
     * @param array $params view parameters
     * @param string $tplName template name
     * @param string $overrideVew name of twig view to override, the $overrideVew must be in the same directory that $view
     * @return string
     * @throws \Twig\Error\Error
     */
    public function renderTemplate($view, array $params = [], string $tplName = '', string $overrideVew = '')
    {
        return $this->twig->render($this->buildTemplatePath($view, $overrideVew, $tplName), $params);
    }

    /**
     * @param string $view path to twig view
     * @param array $params view parameters
     * @param string $scope template scope
     * @param string $overrideVew name of twig view to override, the $overrideVew must be in the same directory that $view
     * @return Response
     * @throws \Twig\Error\Error
     */
    public function renderScope($view, array $params = [], string $scope, string $overrideVew = ''): Response
    {
        $tpl = $this->getTemplateObject($scope);
        return new Response($this->twig->render($this->buildTemplatePath($view, $overrideVew, $tpl->getAlias()), array_merge($params,array("template"=>$tpl))));
    }

    /**
     * @param string $view path to twig view
     * @param array $params view parameters
     * @param string $tplName template name
     * @param string $overrideVew name of twig view to override, the $overrideVew must be in the same directory that $view     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Twig\Error\Error
     */
    public function render($view, array $params = [], string $tplName = '', string $overrideVew = ''): Response
    {
        return new Response($this->renderTemplate($view, $params, $tplName, $overrideVew));
    }

    /**
     * @param string $scope
     * @return QbaBitTemplateInterface
     */
    public function getTemplateObject($scope): QbaBitTemplateInterface
    {

        return new QbTemplate();
    }
}