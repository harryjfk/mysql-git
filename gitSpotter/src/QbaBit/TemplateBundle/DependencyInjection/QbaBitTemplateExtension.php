<?php
/**
 * Created by PhpStorm.
 * User: cronk
 * Date: 3/11/18
 * Time: 11:18 AM
 */

namespace QbaBit\TemplateBundle\DependencyInjection;


use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class QbaBitTemplateExtension extends Extension
{

    /**
     * Loads a specific configuration.
     *
     * @throws \InvalidArgumentException When provided tag is not defined in this extension
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        $loader->load('parameters.yml');

        foreach ($config as $k=>$v){
            $container->setParameter(sprintf('qba_bit_template.%s', $k), $v);
        }

        $path = $container->getParameter('qba_bit_template.path_template_views');
        $directories = new \DirectoryIterator($path);
        foreach ($directories as $dir){

            if(in_array($dir->getBasename(), ['.', '..']) or !$dir->isDir()){
                continue;
            }

            $parsedPath = sprintf('%s/Services/', $dir->getPathname());
            $otherLoader = new Loader\YamlFileLoader($container, new FileLocator($parsedPath));
            if(is_dir($parsedPath)){
                $otherLoader->load('services.yml');
            }

        }
    }
}