<?php
/**
 * Created by PhpStorm.
 * User: cronk
 * Date: 3/25/18
 * Time: 5:39 PM
 */

namespace QbaBit\TemplateBundle\Libs\Interfaces;


interface QbaBitTemplateInterface
{
    /**
     * @return string name of template folder
    */
    public function getAlias(): string;

    /**
     * @return string scope of template ie: front, backend, etc, etc...
     */
    public function getScope(): string;

}