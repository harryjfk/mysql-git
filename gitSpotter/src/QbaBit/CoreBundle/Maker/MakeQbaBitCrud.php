<?php
/**
 * Created by PhpStorm.
 * User: cronk
 * Date: 3/27/18
 * Time: 12:55 PM
 */

namespace QbaBit\CoreBundle\Maker;


use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use Doctrine\Common\Inflector\Inflector;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\MakerBundle\ConsoleStyle;
use Symfony\Bundle\MakerBundle\DependencyBuilder;
use Symfony\Bundle\MakerBundle\Doctrine\DoctrineEntityHelper;
use Symfony\Bundle\MakerBundle\Generator;
use Symfony\Bundle\MakerBundle\InputConfiguration;
use Symfony\Bundle\MakerBundle\Maker\AbstractMaker;
use Symfony\Bundle\MakerBundle\Str;
use Symfony\Bundle\MakerBundle\Validator;
use Symfony\Bundle\TwigBundle\TwigBundle;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Security\Csrf\CsrfTokenManager;
use Symfony\Component\Validator\Validation;

class MakeQbaBitCrud extends AbstractMaker
{
    private $entityHelper;
    private $prefixNameSpaceCompatibilityMode;
    private $pathToOverrideTemplate;

    public function __construct(DoctrineEntityHelper $entityHelper)
    {
        $this->entityHelper = $entityHelper;
        $this->prefixNameSpaceCompatibilityMode = '';
        $this->pathToOverrideTemplate = __DIR__.'/../../../../templates/bundles/MakerBundle/skeleton/';
    }

    public static function getCommandName(): string
    {
        return 'make:qbabit:crud';
    }

    /**
     * {@inheritdoc}
     */
    public function configureCommand(Command $command, InputConfiguration $inputConfig)
    {
        $command
            ->setDescription('Creates CRUD for Doctrine entity class')
            ->addArgument('entity-class', InputArgument::OPTIONAL, sprintf('The class name of the entity to create CRUD (e.g. <fg=yellow>%s</>)', Str::asClassName(Str::getRandomTerm())))
            ->setHelp(file_get_contents(__DIR__ . '/../Resources/help/MakeCrud.txt'));

        $inputConfig->setArgumentAsNonInteractive('entity-class');
    }

    public function interact(InputInterface $input, ConsoleStyle $io, Command $command)
    {
        if (null === $input->getArgument('entity-class')) {
            $argument = $command->getDefinition()->getArgument('entity-class');

            $entities = $this->entityHelper->getEntitiesForAutocomplete();

            $question = new Question($argument->getDescription());
            $question->setAutocompleterValues($entities);

            $value = $io->askQuestion($question);

            $input->setArgument('entity-class', $value);
        }
    }

    private function parseNamespace($name, array $autocompletedNamespaces): array
    {
        if (0 !== strpos($name, '\\')) {
            return $autocompletedNamespaces;
        }
        foreach ($autocompletedNamespaces as $index => $namespace) {
            $parsed = sprintf('\\%s', $namespace);
            if (class_exists($parsed)) {
                $autocompletedNamespaces[$index] = $parsed;
            }
        }
        return $autocompletedNamespaces;
    }

    private function detectCompatibility($name)
    {
        if (0 !== strpos($name, '\\')) {
            return;
        }

        $pars = explode('\\', $name);
        $prefixNamespace = [];
        $count = count($pars);
        for($i = $count; $i >= 0; --$i ){
           if($i >= $count-2){
               continue;
           }
           if($pars[$i]) {
               $prefixNamespace[] = $pars[$i];
           }
        }
        $this->prefixNameSpaceCompatibilityMode = sprintf('%s\\',implode('\\', array_reverse($prefixNamespace)));
    }

    public function generate(InputInterface $input, ConsoleStyle $io, Generator $generator)
    {

        $autocomplete = $this->parseNamespace($input->getArgument('entity-class'), $this->entityHelper->getEntitiesForAutocomplete());
        $this->detectCompatibility($input->getArgument('entity-class'));


        $entityClassDetails = $generator->createClassNameDetails(
            Validator::entityExists($input->getArgument('entity-class'), $autocomplete),
            $this->prefixNameSpaceCompatibilityMode.'Entity\\'

        );

        $entityDoctrineDetails = $this->entityHelper->createDoctrineDetails($entityClassDetails->getFullName());
        $repositoryVars = [];

        if (null !== $entityDoctrineDetails->getRepositoryClass()) {
            $repositoryClassDetails = $generator->createClassNameDetails(
                '\\' . $entityDoctrineDetails->getRepositoryClass(),
                'Repository\\',
                'Repository'
            );

            $repositoryVars = [
                'repository_full_class_name' => $repositoryClassDetails->getShortName(),
                'repository_class_name' => $repositoryClassDetails->getShortName(),
                'repository_var' => lcfirst(Inflector::singularize($repositoryClassDetails->getShortName())),
            ];
        }

        $controllerClassDetails = $generator->createClassNameDetails(
            $entityClassDetails->getShortName(),
            'Controller\\',
            'Controller'
        );

        $iter = 0;
        do {

            $nameFormClass = $entityClassDetails->getRelativeNameWithoutSuffix();
            if($this->prefixNameSpaceCompatibilityMode){
                $nameFormClass = $entityClassDetails->getShortName();
            }

            $formClassDetails = $generator->createClassNameDetails(
                $nameFormClass . ($iter ?: ''),
                $this->prefixNameSpaceCompatibilityMode.'Form\\',
                'Type'
            );
            ++$iter;

        } while (class_exists($formClassDetails->getFullName()));


        $entityVarPlural = lcfirst(Inflector::pluralize($entityClassDetails->getShortName()));
        $entityVarSingular = lcfirst(Inflector::singularize($entityClassDetails->getShortName()));

        $entityTwigVarPlural = Str::asTwigVariable($entityVarPlural);
        $entityTwigVarSingular = Str::asTwigVariable($entityVarSingular);

        $routeName = str_replace('_controller', '', Str::asRouteName($controllerClassDetails->getShortName()));

        $generator->generateClass(
            $this->prefixNameSpaceCompatibilityMode ? $this->prefixNameSpaceCompatibilityMode.'Controller\\'.$controllerClassDetails->getShortName() : $controllerClassDetails->getFullName(),
            $this->prefixNameSpaceCompatibilityMode ? $this->pathToOverrideTemplate.'crud/controller/Controller.tpl.php' : 'crud/controller/Controller.tpl.php',
            array_merge([
                'entity_full_class_name' => $entityClassDetails->getFullName(),
                'entity_class_name' => $entityClassDetails->getShortName(),
                'form_full_class_name' => $this->prefixNameSpaceCompatibilityMode ? $this->prefixNameSpaceCompatibilityMode.'Form\\'.$formClassDetails->getShortName() : $formClassDetails->getFullName(),
                'form_class_name' => $formClassDetails->getShortName(),
                'route_path' => str_replace('/controller', '', Str::asRoutePath($controllerClassDetails->getShortName())),
                'route_name' => $routeName,
                'entity_var_plural' => $entityVarPlural,
                'entity_twig_var_plural' => $entityTwigVarPlural,
                'entity_var_singular' => $entityVarSingular,
                'entity_twig_var_singular' => $entityTwigVarSingular,
                'entity_identifier' => $entityDoctrineDetails->getIdentifier(),
                'prefixAction'=>$this->prefixNameSpaceCompatibilityMode ? 'Action' : '',
                'pathTwigView'=>$this->prefixNameSpaceCompatibilityMode ? sprintf('@%s/%s', preg_replace("/\\\\|Bundle/", '', $this->prefixNameSpaceCompatibilityMode), $entityClassDetails->getShortName()) : '',
            ],
                $repositoryVars
            )
        );

        $generator->generateClass(
            $this->prefixNameSpaceCompatibilityMode ? $this->prefixNameSpaceCompatibilityMode.'Form\\'.$formClassDetails->getShortName() : $formClassDetails->getFullName(),
            'form/Type.tpl.php',
            [
                'bounded_full_class_name' => $this->prefixNameSpaceCompatibilityMode ? $this->prefixNameSpaceCompatibilityMode.'Entity\\'.$entityClassDetails->getShortName() : $entityClassDetails->getShortName(),
                'bounded_class_name' => $entityClassDetails->getShortName(),
                'form_fields' => $entityDoctrineDetails->getFormFields(),
            ]
        );

        $templatesPath = Str::asFilePath($controllerClassDetails->getShortName());

        $templates = [
            '_delete_form' => [
                'route_name' => $routeName,
                'entity_twig_var_singular' => $entityTwigVarSingular,
                'entity_identifier' => $entityDoctrineDetails->getIdentifier(),
            ],
            '_form' => [
                'entity_class_name'=>$entityClassDetails->getShortName(),
                'entity_fields' => $entityDoctrineDetails->getDisplayFields(),
                'entity_identifier' => $entityDoctrineDetails->getIdentifier(),
                'route_name' => $routeName,
            ],
            'edit' => [
                'entity_class_name' => $entityClassDetails->getShortName(),
                'entity_twig_var_singular' => $entityTwigVarSingular,
                'entity_identifier' => $entityDoctrineDetails->getIdentifier(),
                'route_name' => $routeName,
                'pathToView'=>$this->prefixNameSpaceCompatibilityMode ? sprintf('@%s/%s', preg_replace("/\\\\|Bundle/", '', $this->prefixNameSpaceCompatibilityMode), $entityClassDetails->getShortName()) : $entityClassDetails->getShortName()
            ],
            'index' => [
                'entity_class_name' => $entityClassDetails->getShortName(),
                'entity_twig_var_plural' => $entityTwigVarPlural,
                'entity_twig_var_singular' => $entityTwigVarSingular,
                'entity_identifier' => $entityDoctrineDetails->getIdentifier(),
                'entity_fields' => $entityDoctrineDetails->getDisplayFields(),
                'route_name' => $routeName,
            ],
            'new' => [
                'entity_class_name' => $entityClassDetails->getShortName(),
                'route_name' => $routeName,
                'pathToView'=>$this->prefixNameSpaceCompatibilityMode ? sprintf('@%s/%s', preg_replace("/\\\\|Bundle/", '', $this->prefixNameSpaceCompatibilityMode), $entityClassDetails->getShortName()) : $entityClassDetails->getShortName()
            ],
            'show' => [
                'entity_class_name' => $entityClassDetails->getShortName(),
                'entity_twig_var_singular' => $entityTwigVarSingular,
                'entity_identifier' => $entityDoctrineDetails->getIdentifier(),
                'entity_fields' => $entityDoctrineDetails->getDisplayFields(),
                'route_name' => $routeName,
            ],
        ];

        $pathToTemplate = 'templates/%s/%s.html.twig';
        $pathToCrudTemplates = 'crud/templates/%s.tpl.php';
        if($this->prefixNameSpaceCompatibilityMode){
            $pathToTemplate = 'src/'.str_replace('\\', '/', $this->prefixNameSpaceCompatibilityMode).'Resources/views/%s/%s.html.twig';
            $pathToCrudTemplates = $this->pathToOverrideTemplate.'crud/templates/%s.tpl.php';
        }

        foreach ($templates as $template => $variables) {
            $generator->generateFile(
                sprintf($pathToTemplate, $this->prefixNameSpaceCompatibilityMode ? $entityClassDetails->getShortName() : $template, $template),
                sprintf($pathToCrudTemplates, $template),
                $variables
            );
        }


        $generator->writeChanges();

        $this->writeSuccessMessage($io);

        $io->text(sprintf('Next: Check your new CRUD by going to <fg=yellow>%s/</>', Str::asRoutePath($controllerClassDetails->getRelativeNameWithoutSuffix())));
    }

    /**
     * {@inheritdoc}
     */
    public function configureDependencies(DependencyBuilder $dependencies)
    {
        $dependencies->addClassDependency(
            Route::class,
            'annotations'
        );

        $dependencies->addClassDependency(
            AbstractType::class,
            'form'
        );

        $dependencies->addClassDependency(
            Validation::class,
            'validator'
        );

        $dependencies->addClassDependency(
            TwigBundle::class,
            'twig-bundle'
        );

        $dependencies->addClassDependency(
            DoctrineBundle::class,
            'orm-pack'
        );

        $dependencies->addClassDependency(
            CsrfTokenManager::class,
            'security-csrf'
        );
    }
}