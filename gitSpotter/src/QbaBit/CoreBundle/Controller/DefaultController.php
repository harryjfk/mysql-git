<?php

namespace QbaBit\CoreBundle\Controller;

use QbaBit\CoreBundle\Subscriber\QbaBitKernelEventSuscriber;
use QbaBit\MenuBundle\Entity\QbMenu;
use QbaBit\MenuBundle\Entity\QbMenuItems;
use QbaBit\MenuBundle\Libs\Embeddable\QbaBitMenuItemsServiceConfiguration;
use QbaBit\MenuBundle\Libs\Embeddable\QbMenuItemsConfiguration;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{

    /**
     * @Route("/qbabit", name="home")
     */
    public function indexAction()
    {
//        $container = $this->get('service_container');
//        QbaBitKernelEventSuscriber::addCallback(function () use ($container) {
//
//            $container->get('qbabit.template');
//
//            $register = $container->get('qbabit.register.bundle');
//            $register->registerBundle(['QbaBit\LanguageBundle\QbaBitLanguageBundle'])
//                ->saveConfiguration()
//                // ->assetsInstall()
//                ->cacheClear();
//
//        });


        return new Response("Controlador qbabit core...");

//        $em = $this->getDoctrine()->getManager();
//        $list = $em->getRepository('QbaBitCoreBundle:SecurityUser')->findAll();
//        $menu = $em->getRepository('QbaBitMenuBundle:QbMenu')->findOneBy(['id'=>7]);
//
//        $menu = new QbMenu();
//        $menu->setName('Main');
//        $menu->setSlug('main');
//        $menu->setOrder(1);
//        $em->persist($menu);

//        $config = new QbMenuItemsConfiguration();
//        $config->setRoute('qb_route');
//        $config->setRouteParams(['slug'=>'pepe', 'id'=>50]);
//        $config->setPermissions(['ROLE_X', 'ROL_Y', 'ROLE_Z']);
//
//
//        $menuItem = new QbMenuItems();
//        $menuItem->setName('Test');
//        $menuItem->setMenu($menu);
//        $menuItem->setSlug('test');
//        $menuItem->setMetadata($config);
//        $menuItem->setOrder(1);
//        $em->persist($menuItem);
//        $em->flush();


//        $repo = $em->getRepository('QbaBitMenuBundle:QbMenuItems');
//        $loadedMenuItem = $em->getRepository('QbaBitMenuBundle:QbMenuItems')->findOneBy(['id'=>6]);

//        $config = new QbMenuItemsConfiguration();
//        $config->setRoute('core_template');
//        $config->setRouteParams(['slug'=>'pepesd', 'id'=>530]);
//        $config->setPermissions(['ROLE_Z']);
//        $config->setRenderMode('root');
//        $config->setServiceName('qbabit.adminlte.render_item_menu');
//
//        $menuItem = new QbMenuItems();
//        $menuItem->setName('Otroxxxq Child');
//        $menuItem->setMenu($menu);
//        $menuItem->setSlug('otroxxq-child');
//        $menuItem->setMetadata($config);
//        $menuItem->setOrder(1);
//        $menuItem->setParent($loadedMenuItem);
//        $em->persist($menuItem);
//
//        $config = new QbMenuItemsConfiguration();
//        $config->setRoute('core_template');
//        $config->setRouteParams(['slug'=>'pepesd', 'id'=>530]);
//        $config->setPermissions(['ROLE_Z']);
//        $config->setRenderMode('childs');
//        $config->setServiceName('qbabit.adminlte.render_item_menu');
//
//        $menuItem = new QbMenuItems();
//        $menuItem->setName('Otroxxxq Child');
//        $menuItem->setMenu($menu);
//        $menuItem->setSlug('otroxxqs-child');
//        $menuItem->setMetadata($config);
//        $menuItem->setOrder(1);
//        $menuItem->setParent($loadedMenuItem);
//
//        $em->persist($menuItem);
//        $em->flush();


        //return new Response("Respueta desde core...".$this->get('qbabit.test.service')->getContainer()->getParameter('locale'));
        //return $this->render('@QbaBitCore/Default/default.html.twig', ['test'=>'testing!']);
//        return $this->render('@QbaBitCore/Default/default.html.twig', ['test'=>'testing!',
//            'list'=>$list,
//            'itemMenu'=>$loadedMenuItem,
//            'items'=>$repo->getMenuByParent($menu, null)
//            ]);
        //return $this->render('', array('name' => $name));
    }
}
