<?php
/**
 * Created by PhpStorm.
 * User: cronk
 * Date: 4/25/18
 * Time: 6:44 PM
 */

namespace QbaBit\CoreBundle\Subscriber;


use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class QbaBitKernelEventSuscriber implements EventSubscriberInterface
{
    /**
     * @var callable[]
    */
    private static $callbacks = [];

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return array(
            'kernel.terminate' => array('execCallbacks', 300)
        );
    }

    public function execCallbacks(Event $event)
    {
        foreach (self::$callbacks as $callback){
            if(!is_callable($callback)){
                throw new \Exception("Callback is not valid.");
            }

            $callback();
        }
    }

    public static function addCallback(callable $callback)
    {
        self::$callbacks[] = $callback;
    }
}