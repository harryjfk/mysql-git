<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 17/04/18
 * Time: 17:49
 */

namespace QbaBit\CoreBundle\Subscriber;


use Knp\Component\Pager\Event\ItemsEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class RestPaginatorSubscriber implements EventSubscriberInterface

{

    public function items(ItemsEvent $event)
    {
        if (is_object($event->target))
            if (get_class($event->target) == "QbaBit\CoreBundle\Core\Classes\RestBinder") {

                $data = $event->target->getData();
                $event->count = $data["pages"];
                $event->items = array_slice(
                    $data["data"],
                    $event->getOffset(),
                    $event->getLimit()
                );
                //$data["currentPage"],
                //   $data["itemsPerPage"]
                $event->stopPropagation();
            }
    }

    public static function getSubscribedEvents()
    {
        return array(
            'knp_pager.items' => array('items', 1/*increased priority to override any internal*/)
        );
    }
}