<?php
/**
 * Created by PhpStorm.
 * User: cronk
 * Date: 4/24/18
 * Time: 5:57 PM
 * http://symfony.com/doc/current/routing/custom_route_loader.html
 */

namespace QbaBit\CoreBundle\Services;

use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\RouteCollection;

class QbaBitRoutingLoader extends Loader
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
    */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Loads a resource.
     *
     * @param mixed $resource The resource
     * @param string|null $type The resource type or null if unknown
     *
     * @return RouteCollection
     * @throws \Exception If something went wrong
     */
    public function load($resource, $type = null)
    {

        $routes = new RouteCollection();
        $resource = '@QbaBitLanguageBundle/Resources/config/rt.yml';
        $type = 'yaml';

        $importedRoutes = $this->import($resource, $type);
        $routes->addCollection($importedRoutes);

        return $routes;
    }

    /**
     * Returns whether this class supports the given resource.
     *
     * @param mixed $resource A resource
     * @param string|null $type The resource type or null if unknown
     *
     * @return bool True if this class supports the given resource, false otherwise
     */
    public function supports($resource, $type = null)
    {
        return 'qbabit' === $type;
    }
}