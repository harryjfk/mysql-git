<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 4/05/18
 * Time: 2:42
 */

namespace QbaBit\CoreBundle\Services;


use QbaBit\CoreBundle\Core\Traits\ServiceContainer;
use QbaBit\CoreBundle\QbaBitCoreBundle;

class BundleUtils
{
    use ServiceContainer;
    public function getQbaBitBundles()
    {
        $modules = $this->container->get("kernel")->getBundles();
        $qbaBitModules = array();
        foreach ($modules as $module )
            if(strpos($module->getName(),"QbaBit")!==false)
                $qbaBitModules[$module->getName()]=$module;
        return $qbaBitModules;
    }
    public function getTests($bundle_name)
    {
        $bundle=  null;
        if(is_string($bundle_name))
        {
            $modules = $this->getQbaBitBundles();
            $bundle = $modules[$bundle_name];
        }
        else
            $bundle = $bundle_name;

        $dir = $bundle->getPath();
        $dir .= '/Tests/';


        $result = array();
        if(is_dir($dir)==false)
            return $result;
        $this->container->get("qbabit.core.file_utils")->getFiles($dir, $result, true);

        $t = array();
        foreach ($result as $r) {
            $v = str_replace($dir . "/", "", $r);
            $f = explode("/", $v);
            if (count($f) == 3) {
                if (array_key_exists($f[0], $t) == false)
                    $t[$f[0]] = array();
                if (array_key_exists($f[1], $t[$f[0]]) == false)
                    $t[$f[0]][$f[1]] = array();
                $t[$f[0]][$f[1]][] = $r;
            }
            unset($f);

        }
        unset($result);
        return $t;
    }

}