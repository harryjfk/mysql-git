<?php
/**
 * Created by PhpStorm.
 * User: cronk
 * Date: 4/25/18
 * Time: 11:28 AM
 * https://gist.github.com/basdek/5501165/revisions
 * https://symfony.com/doc/current/components/filesystem.html
 */

namespace QbaBit\CoreBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;

class QbaBitRegisterBundle
{

    /**
     * @var \Symfony\Component\Filesystem\Filesystem
    */
    private $fileSystem;

    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
    */
    private $container;

    /**
     * @var array
    */
    private $bundles;


    public function __construct(ContainerInterface $container)
    {
        $this->fileSystem = new Filesystem();
        $this->container = $container;
        $this->bundles = require ($container->getParameter('qbabit_core.path_bundles_config'));
    }

    /**
     * @param array $bundles bundles namespace
     * @return QbaBitRegisterBundle
    */
    public function registerBundle(array $bundles)
    {
        foreach ($bundles as $namespace){
            if(!array_key_exists($namespace, $this->bundles)){
                $this->bundles[$namespace] = ['all'=> true];
            }
        }

        return $this;
    }

    /**
     * @param array $bundles bundles namespace
     * @return QbaBitRegisterBundle
     */
    public function unregisterBundle(array $bundles)
    {
        foreach ($bundles as $namespace){
            $resultBundles = [];
            foreach ($this->bundles as $bundle=>$config){
                if($bundle == $namespace){
                    continue;
                }
                $resultBundles[$bundle] = $config;
            }
            $this->bundles = $resultBundles;
        }

        return $this;
    }

    /**
     * @return QbaBitRegisterBundle
     * @throws \Twig\Error\Error
     */
    public function saveConfiguration()
    {
        $strConfig = $this->container->get('templating')->render('@QbaBitCore/Configuration/bundles.html.twig', ['bundles'=>$this->bundles]);
        $this->fileSystem->dumpFile($this->container->getParameter('qbabit_core.path_bundles_config'), $strConfig);
        return $this;
    }

    /**
     * @param array $options, ie: ['command' => 'cache:clear',"--env" => 'prod']
     * @throws \Exception
     */
    public function executeCommand(array $options)
    {
        $kernel = $this->container->get('kernel');
        $application = new \Symfony\Bundle\FrameworkBundle\Console\Application($kernel);
        $application->setAutoExit(false);
        $application->run(new \Symfony\Component\Console\Input\ArrayInput($options));
    }

    /**
     * @return QbaBitRegisterBundle
     * @throws \Exception
     */
    public function assetsInstall()
    {
        $this->executeCommand(['command'=>'assets:install']);
        return $this;
    }

    /**
     * @return QbaBitRegisterBundle
     * @throws \Exception
     */
    public function cacheClear()
    {
        $this->executeCommand(['command'=>'cache:clear']);
        //$this->fileSystem->remove($this->container->get('kernel')->getCacheDir());
        return $this;
    }
}