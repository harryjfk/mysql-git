<?php
/**
 * Created by PhpStorm.
 * User: cronk
 * Date: 3/11/18
 * Time: 11:08 AM
 */

namespace QbaBit\CoreBundle\Services;


use Symfony\Component\DependencyInjection\ContainerInterface;

class MyTestService
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
    */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }
}