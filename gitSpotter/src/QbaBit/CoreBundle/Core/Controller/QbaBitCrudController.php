<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 13/04/18
 * Time: 22:10
 */

namespace QbaBit\CoreBundle\Core\Controller;


use QbaBit\CoreBundle\Core\Controller\QbaBitBaseController;
use QbaBit\CoreBundle\Core\Traits\FormErrorMessagesTrait;
use QbaBit\FormsBundle\Form\Types\Basic\ButtonType;
use QbaBit\FormsBundle\Form\Types\Crud\ShowCrudType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class QbaBitCrudController extends QbaBitBaseController
{
    use FormErrorMessagesTrait;

    protected function additionalRenderParams($crud_mode = "list")
    {

        // $bundle = $this->getBundleConfig();

        return array_merge(parent::additionalRenderParams(), array(/*$bundle["name"] => $bundle["config"], "base_title" => $this->getBasicNameTranslated($crud_mode),*/
            "url" => $this->getUrlRoutes(),
            "role" => $this->getUrlRoles(), "tb_name" => $this->getSingleTableName()


        ));
    }

    protected function getCurrentObject()
    {
        throw new \Exception("Sin implementar");
    }

    protected function getRepository()
    {

        return $this->getDoctrine()->getRepository($this->getCurrentObject());
    }

    //---------Direccionamiento
    protected function getObjectAsRouteURL()
    {

        $t = $this->getObjectAsArrayStr();
        $r = $t[3];
        $r = str_replace("Qb", "qb_", $r);
        $r = strtolower($r);

        return $r;
    }

    protected function getNewUrl()
    {
        return $this->getObjectAsRouteURL() . "_new";
    }


    protected function getDeleteUrl()
    {
        return $this->getObjectAsRouteURL() . "_delete";
    }

    protected function getDeleteListUrl()
    {
        return $this->getObjectAsRouteURL() . "_delete_list";
    }

    protected function getEditUrl()
    {
        return $this->getObjectAsRouteURL() . "_edit";
    }

    protected function getListUrl()
    {
        return $this->getObjectAsRouteURL() . "_index";
    }
    protected function getShowUrl()
    {
        return $this->getObjectAsRouteURL() . "_show";
    }
    //---------Declaracion de vistas

    protected $strArray = null;

    protected function getObjectAsArrayStr()
    {
        if ($this->strArray == null) {
            $t = $this->getCurrentObject();
            $t = str_replace("Bundle", "", $t);
            $t = explode("\\", $t);
            $this->strArray = $t;
        }

        return $this->strArray;
    }

    protected function getObjectAsRouteTwig($type)
    {

        $t = $this->getObjectAsArrayStr();
        $r = "@" . $t[0] . $t[1] . "/" . $t[3] . "/" . $type . ".html.twig";

        unset($t);
        return $r;
    }

    protected function getIndexRender()
    {

        return $this->getObjectAsRouteTwig("index");
    }

    protected function getEditRender()
    {
        return $this->getObjectAsRouteTwig("edit");
    }

    protected function getShowRender()
    {
        return $this->getObjectAsRouteTwig("show");
    }

    protected function getDataName($plural = false)
    {
        $t = $this->getObjectAsArrayStr();
        $r = $t[3];
        if ($plural) {
            if ($r[strlen($r) - 1] == "y")
                $r = substr($r, 0, strlen($r) - 1) . "i";
            else
                if ($r[strlen($r) - 1] == "e")
                    $r = substr($r, 0, strlen($r) - 1);
            $r .= "es";
        }
        unset($t);
        $r = str_replace("Qb", "qb_", $r);
        $r = strtolower($r);
        return $r;
    }

    public function getUrlRoutes()
    {
        return array('new' => $this->getNewUrl(), 'edit' => $this->getEditUrl(), 'delete_list' => $this->getDeleteListUrl(), 'delete' => $this->getDeleteUrl(), "view" => $this->getListUrl());
    }

    public function getUrlRoles()
    {
        return array("new" => $this->getObjectAsAutorizathion("new"), "edit" => $this->getObjectAsAutorizathion("edit"), "delete" => $this->getObjectAsAutorizathion("delete"), "view" => $this->getObjectAsAutorizathion("view"));
    }

    public function getObjectAsAutorizathion($type)
    {
        $t = $this->getObjectAsArrayStr();
        $r = "ROLE_" . strtoupper($t[1]) . "_" . strtoupper($type);
        return $r;
    }

    public function getSingleTableName()
    {
        $t = $this->getObjectAsArrayStr();
        $r = strtolower($t[1]);
        return $r;
    }

    public function indexAction(Request $request): Response
    {

        if (!$this->isGranted($this->getObjectAsAutorizathion("view"))) {
            throw $this->createAccessDeniedException();
        }
        $searchForm = $this->getSearchForm();
        $searchForm->handleRequest($request);
        $params = $this->getFilterConfigurations($searchForm);
        $paginator = $this->get('knp_paginator');
        $data = $paginator->paginate(
            $this->getRepository()
                ->getQuery($params), $request->query->getInt('page', 1), $this->getParameter('qbabit_pagination_total_records'));

        $params["listData"] = array("all" => false, "rows" => $data);
        $searchForm = $this->getSearchForm();
        $searchForm->setData($params);

        return $this->renderQbabitScope($this->getIndexRender(), 'backend', ["searchForm" => $searchForm->createView(), "data" => $data]);

    }

    protected function getFilterConfigurations(FormInterface $form)
    {
        $filter = $form->getData();
        if ($filter == null)
            $filter = array();
        $filter = array_merge($filter, $this->additionalFilterParams());
        return $filter;
    }

    protected function additionalFilterParams()
    {
        return array();
    }

    public function newAction(Request $request): Response
    {

        if (!$this->isGranted($this->getUrlRoles()["new"])) {
            throw $this->createAccessDeniedException();
        }
        $t = $this->getCurrentObject();
        $object = new $t();
        $url = $this->generateUrl($this->getNewUrl(), array('slugSelected' => $request->get('slugSelected')));
        $editForm = $this->getForm($object, $url);
        $result = $this->handleForm($request, $object, $editForm,$this->getCallBacks());
        if ($result == false) {


            return $this->renderQbabitScope($this->getEditRender(), 'backend', [
                'entity' => $object,
                "isNew" => true,
                'form' => $editForm->createView()]);
        }

        return $result;

    }

    /**
     * @param callable[] $callbacks
     * @param string $event name of callback
     * @param Request $request
     * @param mixed $object
     * @return void
    */
    private function handleCallbackHandleForm(array $callbacks, string $event, Request $request, $object)
    {
        if(array_key_exists($event, $callbacks) and is_callable($callbacks[$event])){
            $callbacks[$event]($request, $object);
        }
    }

    /**
     * @param Request $request
     * @param mixed $object
     * @param Form $form
     * @param callable[] $callback type of callables functions, beforeHandleForm, afterHandleForm, beforeFlush, afterFlush. Receive arguments $request and $object
     * @return boolean|JsonResponse
    */
    protected function handleForm(Request $request, $object, Form $form, array $callback = [])
    {
        $isAjax = $request->isXmlHttpRequest();
        $json = array('status' => 1, 'error' => array(), 'stringError' => '');
        $trans = $this->get('translator');


        $this->handleCallbackHandleForm($callback, 'beforeHandleForm', $request, $object);
        $form->handleRequest($request);
        $this->handleCallbackHandleForm($callback, 'afterHandleForm', $request, $object);


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($object);

            $this->handleCallbackHandleForm($callback, 'beforeFlush', $request, $object);
            $em->flush();
            $this->handleCallbackHandleForm($callback, 'afterFlush', $request, $object);

            if ($isAjax) {
                return new JsonResponse($json);
            } else {
                $this->get('session')->getFlashBag()->add('alert alert-success', $trans->trans('js_datos_guardados', array(), 'corerror'));
                return $this->redirectToRoute($this->getListUrl(), array('slugSelected' => $request->get('slugSelected')));
            }

        } elseif ($form->isSubmitted()) {
            if ($isAjax) {
                $json['status'] = 0;
                $json['error'] = $this->getFormErrorMessages($form);
                return new JsonResponse($json);
            } else {
                $this->get('session')->getFlashBag()->add('alert alert-danger', $trans->trans('js_datos_no_guardados', array(), 'corerror'));
            }
        }
        return false;
    }

    public function getMethod()
    {
        return "POST";
    }


    protected function getFormType()
    {
        //QbCurrencyType::class
        $t = $this->getCurrentObject();
        $t = str_replace("Entity", "Form", $t) . "Type";

        return $t;
    }

    protected function getForm($entity, $url)
    {
        /*  if ($this->get('kernel')->getEnvironment() == "test")
              $form_params["csrf_protection"] = false;*/
        $form = $this->createForm($this->getFormType(), $entity, [
            'method' => $this->getMethod(),
            'action' => $url
        ]);

        $form->add('submit', SubmitType::class, array('label' => 'save', "translation_domain" => "qbabitcore", 'attr' => ['class' => 'btn btn-primary']));
        if ($this->isGranted($this->getObjectAsAutorizathion("edit")))
            $form->add('back', ButtonType::class, array('label' => false,"caption"=>"cancel","buttonType"=>"button", "translation_domain" => "qbabitcore", 'attr' => ['class' => 'btn ']));
        return $form;

    }
    protected function deleteList(array $listIds): array
    {
        $json = array('status' => 1, 'error' => array(), 'stringError' => '',"ids"=>array());
        $repo = $this->getRepository();
        foreach ($listIds as $row => $v)
            $entity = $repo->find($v);
        if ($entity) {
            try {
              $deleted=  $this->delete($entity);
               if($deleted)
                $json['ids'][] = $v;
            } catch (\Exception $e) {
            }

        }

        if (!count($json['ids'])) {
            $json['status'] = 0;
        }
        return $json;
    }
    protected function getSearchFormName()
    {
       $f = $this->getSearchForm();

      return ($f->getName());
    }

    protected function getCheckedList(Request $request)
    {


        $listIds = $request->get($this->getSearchFormName(), array());
        if (array_key_exists("listData", $listIds))
            $listIds = $listIds["listData"]["checked"];
        return $listIds;
    }
    public function deleteListAction(Request $request): Response
    {

        if (!$this->isGranted($this->getUrlRoles()["delete"])) {
            throw $this->createAccessDeniedException();
        }
        $listIds = $this->getCheckedList($request);
        return new JsonResponse($this->deleteList($listIds));


    }

    protected function getCallBacks()
    {
        return array();
    }
    public function editAction(Request $request, $id): Response
    {
        if (!$this->isGranted($this->getUrlRoles()["edit"])) {
            throw $this->createAccessDeniedException();
        }
        $entity = $this->getRepository()->find($id);
        $url = $this->generateUrl($this->getEditUrl(), array("id" => $id, 'slugSelected' => $request->get('slugSelected')));
        $editForm = $this->getForm($entity, $url);
        $result = $this->handleForm($request, $entity, $editForm,$this->getCallBacks());
        if ($result == false) {


            return $this->renderQbabitScope($this->getEditRender(), 'backend', [
                'entity' => $entity,
                "isNew" => false,
                'form' => $editForm->createView()]);
        }

        return $result;

    }

    public function showAction($id): Response
    {
        if (!$this->isGranted($this->getUrlRoles()["show"])) {
            throw $this->createAccessDeniedException();
        }
        $entity = $this->getRepository()->find($id);
        $form = $this->createForm(ShowCrudType::class, $entity, [
            'method' => "DELETE",
            'action' => $this->generateUrl($this->getDeleteUrl(), array("id" => $id))
        ]);
        return $this->renderQbabitScope($this->getShowRender(), 'backend', [
            'entity' => $entity,"deleteForm"=>$form->createView()]);
        //  $this->render($this->getShowRender(), ['entity' => $qbCurrency]);
    }

    public function deleteAction($id): Response
    {
        $object = $this->getRepository()->find($id);
        $this->delete($object);
        return $this->redirectToRoute($this->getListUrl());

    }

    protected function delete($object)
    {
        $t = $this->canBeDeleted($object);
        if (is_bool($t))
        if($t==true)
        {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->remove($object);
                $em->flush();
                return true;
            } catch (\Exception $e) {
                return false;
            }


        }
        return $t;
    }

    protected function canBeDeleted($object)
    {
        return true;
    }


    /*

     //---------Metodos para Pruebas

     public function doTest(Request $request, Form $form)
     {
         if ($this->get('kernel')->getEnvironment() == "test") {


             if ($request->get($form->getName()) != null) {
                 $reflection = new Reflection($form->getData());
                 $traits = $reflection->getTraitNames();
                 $t = $request->get($form->getName());
                 if (array_search('QbaBit\CoreBundle\Traits\FileUploadable', $traits) !== false) {
                     $files = $form->getData()->getFileVar();

                     foreach ($files as $k => $file)

                     {

                         if(array_key_exists($k."_file",$t)!==false)
                             $t[$k . '_file'] = $request->files->get($form->getName())[$k . '_file'];

                     }

                 }
                 $form->submit($t);
                 //  var_dump( $form->getErrors()[0]->getMessage());
             }
         }

     }



     //---------Metodos de gestion

     public function getBasicNameTranslated($crud_mode = "list",$literal=false)

     {
         $r = "";
         $t = str_replace("App\\","",$this->getCurrentObject());
         $t = explode("\\", $t);
         for ($i = 0; $i < count($t); $i++) {
             $s = $this->get("qba_bit_core.class.utils")->getSeparatedNames($t[$i], "_");

             $s = str_replace("_bundle", "", $s);
             if ($s != "entity") {
                 if (strpos($r, $s) === false) {

                     $r .= $s;
                     if ($i < count($t) - 1)
                         $r .= ".";
                 }
             }


         }

         if($r[strlen($r)-1]!=".")
             $r.='.';
         if($literal==false)
         {
             if ($crud_mode == "list")
                 $r .= "text";
             else
                 if ($crud_mode == "edit")
                     $r .= "single";
         }
         else
         {
             $r.="action.".$crud_mode;
             //qbabit.security.admin.groups.actions.edit
         }

         return $r;
     }

     protected function getBundleConfig($name=null)
     {

         $t = explode("\\", $this->getCurrentObject());

         if($name==null)
             $name = $this->container->get("qba_bit_core.class.utils")->getSeparatedNames(str_replace("Bundle", "", $t[2]),"_");
         $bundle = $this->get("qba_bit_core.global.utils")->getQbaBitModules(array($name));

         return array("name" => $name, "config" => $bundle->getConfig());
     }





     public function getObjectAsAutorizathion($type)
     {
         $r = "";
         $t=  $this->getCurrentObject();
         $t = str_replace("App\\","",$t);
         $t = explode("\\",$t);
         for ($i = 0; $i < count($t); $i++) {
             $s = $this->get("qba_bit_core.class.utils")->getSeparatedNames($t[$i], "_");

             $s = str_replace("_bundle", "", $s);
             if ($s != "entity") {
                 $r .= $s;
                 if ($i < count($t) - 1)
                     $r .= "_";
             }


         }
         $r = "ROLE_" . strtoupper($r) . "_" . strtoupper($type);
         return $r;
     }


    */


    /*   protected function getFilterConfigurations(FormInterface $form)
       {
           $filter = $form->getData();
           if ($filter == null)
               $filter = array();
           $filter = array_merge($filter, $this->additionalFilterParams());
           return $filter;
       }


       protected function delete($object)
       {
           $t = $this->canBeDeleted($object);
           if (is_bool($t)) {
               try {
                   $em = $this->get('doctrine.orm.default_entity_manager');
                   $em->remove($object);
                   $em->flush();
                   return true;
               } catch (\Exception $e) {
                   return false;
               }


           }
           return $t;
       }


       public function deleteAction($id, $view_list = false)
       {
           $object = $this->getRepository()->find($id);
           $this->delete($object);
           return $this->redirectToRoute($this->getListUrl(), array('view' => $view_list));

       }




       protected function preEditRender($object)
       {

       }
       protected function preValidated($object,$form)
       {

       }

       protected function preFlush($object, EntityManager $em)
       {

       }

       protected function postFlush($object)
       {

       }
       protected function preHandled($object)
       {

       }



       protected function canBeDeleted($object)
       {
           return true;
       }
       public function deleteListAction(Request $request)
       {

           $json = array('status' => 1);
           $trans = $this->get('translator');

             if (!$this->isGranted($this->getObjectAsAutorizathion("delete"))) {
                 $json['status'] = 2;
                 return new JsonResponse(($json));
             }

           $em = $this->get('doctrine.orm.default_entity_manager');

           $listIds = $request->get('idcheck', array());
           $json['ids'] = array();
           $haeliminado = false;
           foreach ($listIds as $id) {
               $entity = $this->getRepository()->find($id);


               if ($entity && $this->canBeDeleted($entity)) {
                   try {
                       $t = $this->delete($entity);

                       if (is_bool($t)) {
                           $json['ids'][] = $id;

                           $haeliminado = true;
                       } else
                           $json['msg'] = $t;
                   } catch (\Exception $e) {
                       $json['status'] = 0;
                   }
               }
           }

           if (!$haeliminado) {


               $json['status'] = 0;

           } else {
               $this->addFlash('alert alert-success', $trans->trans('deleted_success', array()));
           }

           return new JsonResponse($json);
       }
   */

}