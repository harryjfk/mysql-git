<?php

namespace QbaBit\CoreBundle\Core\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class QbaBitBaseController extends Controller
{
    protected function getSearchFormOptions()
    {
        return array();
    }
    protected function getSearchForm()
    {
        $search =   $this->createForm("QbaBit\\FormsBundle\\Form\\Types\\Basic\\SearchType",null,$this->getSearchFormOptions());
        return $search;
    }

    protected function additionalRenderParams()
    {
        return array();
    }

    /**
     * @param string $view twig view
     * @param array $parameters parameters for twig template
     * @param string $tplName template alias
     * @param string $overrideView render template instead of $view
     * @return Response
     * @throws \Twig\Error\Error
     */
    public function renderQbabit(string $view, array $parameters = array(), string $tplName = "", string $overrideView = ""): Response
    {
        return $this->get('qbabit.template')->render($view, array_merge($parameters,$this->additionalRenderParams()), $tplName, $overrideView);
    }

    /**
     * @param string $view twig view
     * @param array $parameters parameters for twig template
     * @param string $scope template scope
     * @param string $overrideView render template instead of $view
     * @return Response
     * @throws \Twig\Error\Error
     */
    public function renderQbabitScope(string $view, string $scope, array $parameters = array(), string $overrideView = ""): Response
    {
        return $this->get('qbabit.template')->renderScope($view, array_merge($parameters,$this->additionalRenderParams()), $scope, $overrideView);
    }
}
