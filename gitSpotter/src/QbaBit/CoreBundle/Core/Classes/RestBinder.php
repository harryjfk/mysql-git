<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 19/04/18
 * Time: 20:11
 */

namespace QbaBit\CoreBundle\Core\Classes;



use GuzzleHttp\Psr7\Response;

class RestBinder
{
    private $data;

    public function __construct(Response $client=null)
    {
        if($client!=null)
        {
            $data  =$client->getBody()->getContents();
            $this->data = json_decode($data,true);
        }
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     * @return RestBinder
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }


}