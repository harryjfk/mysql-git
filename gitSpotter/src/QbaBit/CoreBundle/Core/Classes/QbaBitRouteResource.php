<?php
/**
 * Created by PhpStorm.
 * User: cronk
 * Date: 4/24/18
 * Time: 6:17 PM
 */

namespace QbaBit\CoreBundle\Core\Classes;


use Symfony\Component\Config\Resource\ResourceInterface;

class QbaBitRouteResource implements ResourceInterface
{

    /**
     * @var string
    */
    private $resource;

    public function __construct(string $resource)
    {
        $this->resource = $resource;
    }

    /**
     * Returns a string representation of the Resource.
     *
     * This method is necessary to allow for resource de-duplication, for example by means
     * of array_unique(). The string returned need not have a particular meaning, but has
     * to be identical for different ResourceInterface instances referring to the same
     * resource; and it should be unlikely to collide with that of other, unrelated
     * resource instances.
     *
     * @return string A string representation unique to the underlying Resource
     */
    public function __toString()
    {
        return $this->resource;
    }
}