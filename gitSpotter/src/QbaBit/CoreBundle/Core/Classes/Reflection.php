<?php
/**
 * Created by PhpStorm.
 * User: Harry
 * Date: 12/09/2017
 * Time: 11:44
 */

namespace QbaBit\CoreBundle\Core\Classes;


class Reflection extends \ReflectionClass
{
    /**
     * @return \ReflectionProperty A
     */
    public function getProperty($name)
    {
        $classes = $this->getParentClasses();
        for ($i = 0; $i < count($classes); $i++) {
            $class = $classes[$i];
            if ($i == 0) {
                if ($class->hasInheritedProperty($name))
                    return $class->getInheritedProperty($name);
            } else
                if ($class->hasProperty($name))
                    return $class->getProperty($name);
        }

        return null;
    }

    private function getInheritedProperty($name)
    {
        return parent::getProperty($name);
    }

    private function hasInheritedProperty($name)
    {
        return parent::hasProperty($name);
    }

    /**
     * @return \ReflectionClass []
     */
    public function getTraits()
    {
        $result = array();
        $t = $this;
        $result = array_merge($result, parent::getTraits());
        while ($t->getParentClass() != null) {
            $t = $t->getParentClass();
            $result = array_merge($result, $t->getTraits());
        }
        return $result;
    }

    /**
     * @return array
     */
    public function getTraitNames()
    {
        $result = array();
        $t = $this;
        $result = array_merge($result, parent::getTraitNames());
        while ($t->getParentClass() != null) {
            $t = $t->getParentClass();
            $result = array_merge($result, $t->getTraitNames());
        }
        return $result;
    }

    /**
     * @param null $filter
     * @return \ReflectionMethod[]
     */
    public function getMethods($filter = null)
    {
        $result = array();
        $result = array_merge($result, parent::getMethods());
        $t = $this;
        while ($t->getParentClass() != null) {
            $t = $t->getParentClass();
            $result = array_merge($result, $t->getMethods());
        }

        return $result;


    }

    /**
     * @return \ReflectionClass[]
     */
    public function getParentClasses()
    {
        $result = array();
        $result[] = $this;
        $t = $this;
        while ($t->getParentClass() != null) {
            $t = $t->getParentClass();
            $result[] = $t;
        }

        return $result;


    }

    /**
     * @return array
     */
    public function getParentClassesNames()
    {
        $result = array();
        foreach ($this->getParentClasses() as $class)
            $result[] = $class->getName();


        return $result;


    }

    public function getMethodFromPropertyName($propName)
    {
        if ($this->hasProperty($propName)) {
            $methods = $this->getMethods();
            foreach ($methods as $method) {
                $mName = $method->getName();
                $mName = str_replace("get", "", $mName);
                if (strtolower($mName) == strtolower($propName))
                    return $method;
            }

        }

        return null;
    }

    ///-------------CONFIGURATIONS EMBBEBED
    protected function getCommentAttribute($comment, $attribute)
    {
        $result = array();
        foreach ($comment as $c)
            if (strpos(strtolower($c), "@" . $attribute) !== false) {
                $result[] = trim(str_replace("@" . $attribute, "", str_replace("*", "", trim($c))));

            }
        if (count($result) == 1)
            return $result[0];
        return $result;
    }

    protected function getSrcObject($path, Reflection $reflection, $src_object)
    {
        $path = str_replace('"', "", $path);
        $tree = explode(".", $path);
        $dest = $src_object;
        $searchData = $reflection;
        for ($i = 0; $i < count($tree); $i++) {
            $t = $tree[$i];

            if (is_array($dest)) {
                if (array_key_exists($t, $dest))
                    $temp = $dest[$t];
                else
                    $temp = null;
            } else
                $temp = $searchData->getMethodFromPropertyName($t);
            if ($temp != null) {
                $dest = $temp->invoke($dest);
                if (!is_array($dest))
                    $searchData = new Reflection($dest);
            } else {
                $dest = null;
                break;
            }


        }

        return $dest;

    }

    public static function getConfigurationInheterance($configurationClass, $data = null,$indexed=true)
    {
        $t = new Reflection($configurationClass);
        $properties = $t->getProperties();
        $result = array();
        foreach ($properties as $property) {
            $comment = explode("\n", $property->getDocComment());
            $var_type = $t->getCommentAttribute($comment, "var");
            if ($var_type != "") {
                if ($var_type == "array") {
                    $global = $t->getCommentAttribute($comment, "global");
                    $editable = str_replace('editable("', "", str_replace('")', '', $global[0]));
                    $arrayType = str_replace('arrayType("', "", str_replace('")', '', $global[1]));

                    $result[$property->getName()] = array("editType"=>"array","editable" => $editable, "arrayType" => $arrayType);
                } else
                    if ($var_type == "string") {
                        $global = $t->getCommentAttribute($comment, "global");
                        $type_pos = strpos($global, "(");

                        $edit_type = substr($global, 0, $type_pos);
                        $result[$property->getName()] = array("editType" => $edit_type);

                        if ($edit_type == "select") {
                            $src_select = substr($global, $type_pos + 1, (strlen($global) - $type_pos) - 2);
                            $src_select = str_replace('"', "", $src_select);
                            $result[$property->getName()]["src"] = $src_select;
                            if ($data != null) {
                                $object = $t->getSrcObject($src_select, $t, $data);
                                $choices = array();
                                if (is_array($object))
                                    foreach ($object as $k => $v)
                                        if($indexed==false)

                                            $choices[] = array("id"=> $k,"name"=>$v) ;
                                        else

                                            $choices[$v] = $k;
                                else
                                    if (get_class($object) == "Doctrine\ORM\PersistentCollection") {
                                        foreach ($object as $obj)
                                            if($indexed==false)
                                            {
                                                $choices[] = array("id"=> $obj->getId(),"name"=>$obj->__toString()) ;
                                            }


                                            else
                                                $choices[$obj->__toString()] = $obj->getId();

                                    }
                                $result[$property->getName()]["choices"] = $choices;
                            }

                        }
                    } else
                        if ($var_type == "boolean") {
                            $result[$property->getName()] = array("editType"=>"check");

                        }


            }

        }
        return $result;
    }

}