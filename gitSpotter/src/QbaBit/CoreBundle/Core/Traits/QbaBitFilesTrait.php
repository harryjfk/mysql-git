<?php
/**
 * Created by PhpStorm.
 * User: cronk
 * Date: 4/4/18
 * Time: 12:39 PM
 */

namespace QbaBit\CoreBundle\Core\Traits;


use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

trait QbaBitFilesTrait
{
    public function moveFile(UploadedFile $source, $dest)
    {
        $filename = uniqid() . '.' . $source->guessExtension();
        $source->move($dest, $filename);
        unset($source);
        return $filename;
    }

    public function removeFile($pathToFile)
    {
        $file = new Filesystem();
        if ($file->exists($pathToFile)) {
            $file->remove($pathToFile);
        }
    }
}