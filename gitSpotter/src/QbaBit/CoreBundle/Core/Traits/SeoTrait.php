<?php
/**
 * Created by PhpStorm.
 * User: Harry
 * Date: 31/12/2016
 * Time: 1:43
 */

namespace QbaBit\CoreBundle\Core\Traits;


trait SeoTrait
{
    /**
     * @var string|null
     *
     * @ORM\Column(name="seoTitle", type="text", length=65535, nullable=true, options={"default"="NULL"})
     */
    protected $seotitle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="seoKeyWords", type="text", length=65535, nullable=true, options={"default"="NULL"})
     */
    protected $seokeywords;

    /**
     * @var string|null
     *
     * @ORM\Column(name="seoMetaDescription", type="text", length=65535, nullable=true, options={"default"="NULL"})
     */
    protected $seometadescription;
    /**
     * Set seotitle.
     *
     * @param string|null $seotitle
     *
     * @return SeoTrait
     */
    public function setSeotitle($seotitle = null)
    {
        $this->seotitle = $seotitle;

        return $this;
    }

    /**
     * Get seotitle.
     *
     * @return string|null
     */
    public function getSeotitle()
    {
        return $this->seotitle;
    }

    /**
     * Set seokeywords.
     *
     * @param string|null $seokeywords
     *
     * @return SeoTrait
     */
    public function setSeokeywords($seokeywords = null)
    {
        $this->seokeywords = $seokeywords;

        return $this;
    }

    /**
     * Get seokeywords.
     *
     * @return string|null
     */
    public function getSeokeywords()
    {
        return $this->seokeywords;
    }

    /**
     * Set seometadescription.
     *
     * @param string|null $seometadescription
     *
     * @return SeoTrait
     */
    public function setSeometadescription($seometadescription = null)
    {
        $this->seometadescription = $seometadescription;

        return $this;
    }

    /**
     * Get seometadescription.
     *
     * @return string|null
     */
    public function getSeometadescription()
    {
        return $this->seometadescription;
    }

}