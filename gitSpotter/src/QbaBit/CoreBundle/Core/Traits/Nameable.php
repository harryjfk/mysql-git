<?php
/**
 * Created by PhpStorm.
 * User: Harry
 * Date: 31/12/2016
 * Time: 2:33
 */

namespace QbaBit\CoreBundle\Core\Traits;

/**
 * Class Nameable
 * @package QbaBit\CoreBundle\Traits
 * @fields{name}
 *
 */
trait Nameable {

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    protected $name;
    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Nameable
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    public function __toString()
    {
        return (string)$this->name;
    }
}