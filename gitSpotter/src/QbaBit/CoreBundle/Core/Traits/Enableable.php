<?php
/**
 * Created by PhpStorm.
 * User: Harry
 * Date: 31/12/2016
 * Time: 1:43
 */

namespace QbaBit\CoreBundle\Core\Traits;

/**
 * Class Enableable
 * @package QbaBit\CoreBundle\Traits
 * @fields{enabled}
 *
 */
trait Enableable
{
    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", nullable=false, options={"default"="1"})
     */
    protected $active;

    /**
     * Set active.
     *
     * @param bool $active
     *
     * @return Enableable
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active.
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }
}