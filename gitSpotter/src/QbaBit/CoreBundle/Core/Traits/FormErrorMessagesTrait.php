<?php
/**
 * Created by PhpStorm.
 * User: cronk
 * Date: 4/3/18
 * Time: 8:45 PM
 */

namespace QbaBit\CoreBundle\Core\Traits;


trait FormErrorMessagesTrait
{
    public function getFormErrorMessages(\Symfony\Component\Form\Form $form)
    {
        $errors = array('nameForm' => $form->getName(), 'messages' => array());
        foreach ($form as $field => $child) {
            // Global
            foreach ($form->getErrors() as $error) {
                $errors['messages'][] = array('msg' => $error->getMessage(), 'field' => $field);
            }

            // Fields
            if ($child->isSubmitted() && !$child->isValid()) {
                foreach ($child->getErrors() as $error) {
                    $errors['messages'][] = array('msg' => $error->getMessage(), 'field' => $field);
                }
            }
        }
        return $errors;
    }
}