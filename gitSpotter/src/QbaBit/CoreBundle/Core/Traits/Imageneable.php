<?php
/**
 * Created by PhpStorm.
 * User: Harry
 * Date: 31/12/2016
 * Time: 1:43
 */

namespace QbaBit\CoreBundle\Core\Traits;

/**
 * Class Imageneable
 * @package QbaBit\CoreBundle\Traits
 * @fields{image}
 *
 */
trait Imageneable {
    /**
     * @var string|null
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=false, options={"default"="NULL"})
     */
    protected $image;

    /**
     * @return null|string
     */
    public function getImage()
    {
        return $this->image ? $this->image : 'nouser.jpg';

    }

    /**
     * @param null|string $image
     * @return Imageneable
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

}