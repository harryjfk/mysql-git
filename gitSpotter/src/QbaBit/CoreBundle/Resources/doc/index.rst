#############
Controladores
#############

Todos los controladores deberían heredar del controlador src/QbaBit/CoreBundle/Core/Controller/QbaBitBaseController,
este controlador agrega dos métodos para renderizar la vistas twig, llamados: renderQbabit y renderQbabitScope. Lo
diferente en estos métodos es lo siguiente:

.. code-block:: php
   :linenos:
   /**
    * @var string $tplName indica el código de la plantilla o nombre de la carpeta de la plantilla, por ejemplo
    * adminlte, podría ser el nombre de la carpeta contenedora de la plantilla, dentro de ella se van a buscar
    * las vistas a sobrescribir siempre que estén definidas, de lo contrario usa la del bundle.
    *
    * @var string $overrideView esta sería una vista alternativa a cargar, la cual tiene prioridad sobre $view, si
    * $overrideView existe se renderiza $overrideView en vez de $view
    */
   public function renderQbabit(string $view, array $parameters = array(), string $tplName = "", string $overrideView = "");

   /**
    * @var string $scope, indica el tipo de plantilla a cargar o sea front, backend o el tipo que se le asigne a la
    * plantilla, el método por dentro encuentra la plantilla correspondiente en la base de datos y obtiene el código de
    * la plantilla
    */
   public function renderQbabitScope(string $view, string $scope, array $parameters = array(), string $overrideView = "");

Estos métodos primero verifican si la vista twig está redefinida en src/QbaBit/TemplateBundle/Resources, si es así
renderizan la plantilla, de lo contrario siguen el comportamiento por defecto que tiene symfony. A continuación se
deja un ejemplo de como llamar a estos métodos desde el controlador:

.. code-block:: php
   :linenos:
   //sin definir plantilla renderizará la plantilla por defecto y no buscará redefinición a no ser las que
   //define symfony por defecto
   return $this->renderQbabit('@QbaBitEjemplo/Default/index.html.twig');

   //de esta forma busca si hay alguna redefinición de la vista index.html.twig dentro de la plantilla adminlte
   return $this->renderQbabit('@QbaBitEjemplo/Default/index.html.twig', [], 'adminlte');

   //de esta forma busca si hay otro_index.html.twig a renderizar en la plantilla, si no existe lo busca en el
   //bundle EjemploBundle, si tampoco existe intenta renderizar index.html.twig volviendo a hacer todo el proceso
   return $this->renderQbabit('@QbaBitEjemplo/Default/index.html.twig', [], 'adminlte', 'otro_index.html.twig');

Para el caso de renderQbabitScope funciona exactamente igual que el anterior lo que en vez de pasar el código de
la plantilla se pasa el tipo de la plantilla para que el sistema la identifique automáticamente en la base de datos.