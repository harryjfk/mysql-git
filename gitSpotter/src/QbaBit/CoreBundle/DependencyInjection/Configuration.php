<?php
/**
 * Created by PhpStorm.
 * User: cronk
 * Date: 3/11/18
 * Time: 11:16 AM
 */

namespace QbaBit\CoreBundle\DependencyInjection;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    /**
     * Generates the configuration tree builder.
     *
     * @return \Symfony\Component\Config\Definition\Builder\TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('qbabit_core');
        $rootNode->children()
            ->scalarNode('path_bundles_config')
            ->defaultValue(__DIR__ . '/../../../../config/bundles.php')
            ->end()
            ->end();

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        return $treeBuilder;
    }
}