<?php
/**
 * Created by PhpStorm.
 * User: cronk
 * Date: 3/28/18
 * Time: 10:35 PM
 */

namespace QbaBit\CoreBundle\Libs\Embeddable;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QbLocationConfiguration implements \ArrayAccess
{


    /**
     * @var string
     * @Serializer\Type(name="string")
     */
    private $latitud;
    /**
     * @var string
     * @Serializer\Type(name="string")
     */
    private $longitud;
    /**
     * @var string
     * @Serializer\Type(name="string")
     */
    private $address;


    public function __construct()
    {

        $this->latitud= 37.9397718;
        $this->longitud= 643050899999935;
        $this->address="";
    }

    /**
     * @return string
     */
    public function getLatitud()
    {
        return $this->latitud;
    }

    /**
     * @param string $latitud
     * @return QbLocationConfiguration
     */
    public function setLatitud($latitud)
    {
        $this->latitud = $latitud;
        return $this;
    }

    /**
     * @return string
     */
    public function getLongitud()
    {
        return $this->longitud;
    }

    /**
     * @param string $longitud
     * @return QbLocationConfiguration
     */
    public function setLongitud($longitud)
    {
        $this->longitud = $longitud;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return QbLocationConfiguration
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }




    /**
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists($offset)
    {
        return true;
        // TODO: Implement offsetExists() method.
    }

    /**
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     * @since 5.0.0
     */
    public function offsetGet($offset)
    {
        if($offset=="search")
            return "";
        return $this->$offset;
        // TODO: Implement offsetGet() method.
    }

    /**
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetSet($offset, $value)
    {
        $this->$offset = $value;
        // TODO: Implement offsetSet() method.
    }

    /**
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetUnset($offset)
    {
        // TODO: Implement offsetUnset() method.
    }
}