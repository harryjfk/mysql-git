<?php
/**
 * Created by PhpStorm.
 * User: cronk
 * Date: 3/28/18
 * Time: 10:35 PM
 */

namespace QbaBit\CoreBundle\Libs\Embeddable;

use JMS\Serializer\Annotation as Serializer;
use QbaBit\CoreBundle\Core\Classes\ArrayGetter;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QbSeoConfiguration extends ArrayGetter
{


    /**
     * @var string
     * @Serializer\Type(name="string")
     */
    private $seotitle;
    /**
     * @var string
     * @Serializer\Type(name="string")
     */
    private $seokeywords;
    /**
     * @var string
     * @Serializer\Type(name="string")
     */
    private $seometadescription;


    public function __construct()
    {

        $this->seokeywords= "";
        $this->seometadescription="";
        $this->seotitle="";
    }

    /**
     * @return string
     */
    public function getSeotitle()
    {
        return $this->seotitle;
    }

    /**
     * @param string $seotitle
     * @return QbSeoConfiguration
     */
    public function setSeotitle($seotitle)
    {
        $this->seotitle = $seotitle;
        return $this;
    }

    /**
     * @return string
     */
    public function getSeokeywords()
    {
        return $this->seokeywords;
    }

    /**
     * @param string $seokeywords
     * @return QbSeoConfiguration
     */
    public function setSeokeywords($seokeywords)
    {
        $this->seokeywords = $seokeywords;
        return $this;
    }

    /**
     * @return string
     */
    public function getSeometadescription()
    {
        return $this->seometadescription;
    }

    /**
     * @param string $seometadescription
     * @return QbSeoConfiguration
     */
    public function setSeometadescription($seometadescription)
    {
        $this->seometadescription = $seometadescription;
        return $this;
    }


    /**
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists($offset)
    {
        return true;
        // TODO: Implement offsetExists() method.
    }

    /**
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     * @since 5.0.0
     */
    public function offsetGet($offset)
    {
        if($offset=="search")
            return "";
        return $this->$offset;
        // TODO: Implement offsetGet() method.
    }

    /**
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetSet($offset, $value)
    {
        $this->$offset = $value;
        // TODO: Implement offsetSet() method.
    }

    /**
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetUnset($offset)
    {
        // TODO: Implement offsetUnset() method.
    }

}