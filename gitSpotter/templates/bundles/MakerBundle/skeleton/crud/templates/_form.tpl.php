{{ form_start(form, { attr:{id:form.vars.id} }) }}
<div class="qb-frm">

    <div class="row nmlf">
        <?php $idField = ''; ?>
        <?php foreach ($entity_fields as $field): if ($field['fieldName'] == $entity_identifier) {
            $idField = $field['fieldName'];
            continue;
        } ?>
            <div class="col-md-12">
                {{ form_row(form.<?= $field['fieldName'] ?>, {'attr':{'class': 'form-control'} }) }}
            </div>
        <?php endforeach; ?>
    </div>

    <div class="qb-btn-formater">
        <button type="submit" id="<?= $entity_class_name ?>_submit" name="<?= $entity_class_name ?>[submit]"
                class="btn btn-primary">{{ "save" | trans({}, "qbabitcore") }}
        </button>
        <button type="button" id="<?= $entity_class_name ?>_button" name="<?= $entity_class_name ?>[button]"
                class="btn">{{ "cancel" | trans({}, "qbabitcore") }}
        </button>
    </div>
</div>
{{ form_end(form) }}

<script type="text/javascript">

    function QbabitJavascriptsCupones() {

        jQuery('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });

        jQuery("#{{ form.vars.id }}").on('submit', function (e) {
            e.preventDefault();
            var _this = jQuery(this);

            MKLib.AjaxRequest(_this.attr('action'), _this.attr('id'), function (j) {
                var status = parseInt(j.status);
                if (status === 1) {
                    MKLib.showMsg('alert-success', '{{ "js_datos_guardados" | trans({}, "corerror") }}');
                    <?php if($idField): ?>
                    {% if not form.vars.value.<?=$idField ?> %}
                      MKLib.resetForm("{{ form.vars.id }}");
                    {% endif %}
                    <?php endif; ?>
                } else {
                    if (status === 2) {
                        MKLib.showMsg('alert-danger', '{{ "js_acceso_denegado" | trans({}, "corerror") }}');
                    }
                }
                if (j.stringError) {
                    MKLib.showMsg('alert-danger', j.stringError);
                }
                Util.renderErrors(j.error);
                MKLib.unlockScreen();
            }, 'json');

        });

        jQuery("#<?= $entity_class_name ?>_button").on('click', function () {
            MKLib.AjaxUpdate("{{ path("<?=$route_name?>_index", paramsUrl) | raw }}", 'dvupdate', '');
        });

    }

    {% if app.request.isXmlHttpRequest == false %}
      window.addEventListener('load', function () {
        QbabitJavascriptsCupones();
      }, false);
    {% else %}
     jQuery(function () {
        QbabitJavascriptsCupones();
     });
    {% endif %}
</script>