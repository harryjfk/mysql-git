{% extends app.request.isXmlHttpRequest ? '@QbaBitCore/ajax.html.twig' : QbaBitLayoutToExtend('@QbaBitTemplate/adminlte/layout.html.twig', 'backend') %}

{% set tit = 'list_X' | trans({'%name%': '<?= $entity_class_name ?>' }, 'qbabitcore') %}
{% set paramsUrl = {'slugSelected': app.request.get('slugSelected')} %}
{% set fake = QbaBitSeoTitle(tit) %}

{% block update %}

<div class="box-header with-border">
    <div class="user-block">
            <span class="username" style="margin-left: 0px;"><h3 style="margin-bottom: 0;margin-top: 0;">{{ tit }}</h3></span>
    </div>
    <!-- /.user-block -->
    {% if is_granted('ROLE_X_NEW') or is_granted('ROLE_X_DELETE') %}
    <div class="box-tools">
        <div class="dropdown">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownOptions"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="fa fa-fw fa-cogs"></span>
                {{ 'options' | trans({}, 'qbabitcore') }}
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownOptions">
                <li>
                    {% if is_granted('ROLE_X_NEW') %}
                    <a href="{{ path('<?= $route_name ?>_new', paramsUrl) }}" class="new"
                    style="height: auto;"><i class="fa fa-plus-square"></i>{{ 'new' | trans({}, 'qbabitcore') }}</a>
                    {% endif %}
                </li>
                <li>
                    {% if is_granted('ROLE_X_DELETE') %}
                    <a href="#" class="del"><i class="fa fa-trash-o"></i>{{ 'delete' | trans({}, 'qbabitcore') }}</a>
                    {% endif %}
                </li>
            </ul>
        </div>
    </div>
    {% endif %}
    <!-- /.box-tools -->
</div>

<form action="{{ path('<?= $route_name ?>_index', paramsUrl) }}" name="frmsearch" id="frmsearch" method="post">
<div class="row nmlf">
    <div class="col-md-3">
        <div class="form-group">
            <label>{{ 'name' | trans({}, 'qbabitcore') }}</label>
            <input type="text" id="name" name="name" class="form-control"
                   value="{{ app.request.get('name') }}">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label style="display: block;">&nbsp;</label>
            <button type="submit" class="btn blue">
                <i class="fa fa-search"></i>
            </button>
        </div>
    </div>
</div>

<div class="box-body table-responsive no-padding">
    <table class="table table-hover">
        <thead>

        <tr>
            <th style="width: 25px;">
                <input type="checkbox" id="chkall" class="chkall minimal"/>
            </th>

            <?php foreach ($entity_fields as $field): if($field['fieldName'] == $entity_identifier){continue;} ?>
                <th><?= ucfirst($field['fieldName']) ?></th>
            <?php endforeach; ?>

        </tr>
        </thead>
        <tbody>
        {% for <?= $entity_twig_var_singular ?> in <?= $entity_twig_var_plural ?> %}
        <tr id="rw{{ <?= $entity_twig_var_singular ?>.<?=$entity_identifier?> }}">
            <td>
                <input type="checkbox" name="idcheck[]" value="{{ <?= $entity_twig_var_singular ?>.<?= $entity_identifier ?> }}" id="id{{ <?= $entity_twig_var_singular ?>.<?= $entity_identifier ?> }}" class="selitm minimal"/>
            </td>
            <?php
            $step = 0;
            foreach ($entity_fields as $field): if($field['fieldName'] == $entity_identifier){continue;}?>
              <?php if(!$step): $step = 1; ?>
                    <td>
                        {% if is_granted('ROLE_X_EDIT') %}
                        <a href="{{ path('<?= $route_name ?>_edit', {'<?= $entity_identifier ?>': <?= $entity_twig_var_singular ?>.<?= $entity_identifier ?>}) }}"
                        class="edit">{{ <?= $helper->getEntityFieldPrintCode($entity_twig_var_singular, $field) ?> }}</a>
                        {% else %}
                        {{ <?= $helper->getEntityFieldPrintCode($entity_twig_var_singular, $field) ?> }}
                        {% endif %}
                    </td>
                <?php else: ?>
                <td>{{ <?= $helper->getEntityFieldPrintCode($entity_twig_var_singular, $field) ?> }}</td>
              <?php endif ?>
            <?php endforeach; ?>
        </tr>
        {% else %}
        <tr>
            <td colspan="<?= (count($entity_fields) + 2) ?>">{{ 'no_records_founds' | trans({}, 'qbabitcore') }}</td>
        </tr>
        {% endfor %}
        </tbody>
    </table>
</div>
    <div class="qbcore-tc">{{ knp_pagination_render(<?= $entity_twig_var_plural ?>) }}</div>
</form>

<script type="text/javascript">
    function QbabitJavascripts() {

        jQuery('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        }).trigger('click');//le pongo trigger('click') para corregir bug

        jQuery('#chkall').on('ifToggled', function () {
            MKLib.checkedCtrl(jQuery(this), '.selitm', function (status, ele) {
                ele.iCheck(status ? 'check' : 'uncheck');
            });
        });

        jQuery('.del').on('click', function (e) {
            e.preventDefault();
            var list = jQuery('#frmsearch .selitm:checked');
            if (list.length) {
                if (confirm("{{ 'js_eliminar_seleccionados' | trans({}, 'qbabitcore') }}")) {
                    MKLib.AjaxRequest("{{ path('<?= $route_name ?>_delete') }}", 'frmsearch', function (j) {

                        var status = parseInt(j.status);
                        if (status === 1) {
                            MKLib.deleteItem(j.ids, function () {
                                MKLib.showMsg('alert-success', "{{ 'js_eliminados_correctamente' | trans({}, 'qbabitcore') }}");
                            }, 'rw');
                        } else {
                            if (status === 2)
                                MKLib.showMsg('alert-danger', "{{ 'js_acceso_denegado' | trans({}, 'qbabitcore') }}");
                        else
                            MKLib.showMsg('alert-danger', "{{ 'js_error_eliminar_datos' | trans({}, 'qbabitcore') }}");
                        }
                        MKLib.unlockScreen();

                    }, 'json', 'DELETE');
                }
            } else
                MKLib.showMsg('alert-danger', "{{ 'js_seleccionar_elemento' | trans({}, 'qbabitcore') }}");
        });

        MKAjax.setAjaxUpdateForm('frmsearch', '', 'dvupdate', function () {
            //Materialize.updateTextFields();
        });
        MKAjax.paginate('.pagination a, th a, .edit, .new', 'dvupdate', 'frmsearch');

    }

    {% if app.request.isXmlHttpRequest == false %}
    window.addEventListener('load', function () { QbabitJavascripts(); }, false);
    {% else %}
    jQuery(function () {
        QbabitJavascripts();
    });
    {% endif %}
</script>

{% endblock %}