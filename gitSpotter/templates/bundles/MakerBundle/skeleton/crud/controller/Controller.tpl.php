<?= "<?php\n" ?>

namespace <?= $namespace ?>;

use <?= $entity_full_class_name ?>;
use <?= $form_full_class_name ?>;
<?php if (isset($repository_full_class_name)): ?>
use <?= $repository_full_class_name ?>;
<?php endif ?>
use QbaBit\CoreBundle\Core\Controller\QbaBitBaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use QbaBit\CoreBundle\Core\Traits\FormErrorMessagesTrait;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("<?= $route_path ?>")
 */
class <?= $class_name ?> extends QbaBitBaseController
{
    use FormErrorMessagesTrait;

    /**
     * @Route("/", name="<?= $route_name ?>_index")
     */
<?php if (isset($repository_full_class_name)): ?>
    public function index<?= $prefixAction ?>(<?= $repository_class_name ?> $<?= $repository_var ?>): Response
    {
        return $this->render('<?= $pathTwigView ? $pathTwigView : $route_name ?>/index.html.twig', ['<?= $entity_twig_var_plural ?>' => $<?= $repository_var ?>->findAll()]);
    }
<?php else: ?>
    public function index<?= $prefixAction ?>(Request $request): Response
    {
        if(!$this->isGranted('ROLE_X')){
          throw $this->createAccessDeniedException();
        }

        $paginator = $this->get('knp_paginator');
        $<?= $entity_var_plural ?> = $paginator->paginate($this->getDoctrine()
            ->getRepository(<?= $entity_class_name ?>::class)
            ->findAll(), $request->query->getInt('page', 1), $this->getParameter('qbabit_pagination_total_records'));

        return $this->renderQbabitScope('<?= $pathTwigView ? $pathTwigView : $route_name ?>/index.html.twig', 'backend', ['<?= $entity_twig_var_plural ?>' => $<?= $entity_var_plural ?>]);
    }
<?php endif ?>

    /**
     * @Route("/new", name="<?= $route_name ?>_new", methods="GET|POST")
     */
    public function new<?= $prefixAction ?>(Request $request): Response
    {
        $isAjax = $request->isXmlHttpRequest();
        $json = array('status' => 1, 'error' => array(), 'stringError'=>'');
        $trans = $this->get('translator');

        if(!$this->isGranted('ROLE_X_NEW')){
          throw $this->createAccessDeniedException();
        }

        $<?= $entity_var_singular ?> = new <?= $entity_class_name ?>();
        $form = $this->createForm(<?= $form_class_name ?>::class, $<?= $entity_var_singular ?>,[
        'method'=>'POST',
        'action'=>$this->generateUrl('<?= $route_name ?>_new', array('slugSelected' => $request->get('slugSelected')))
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($<?= $entity_var_singular ?>);
            $em->flush();

            if ($isAjax) {
              return new JsonResponse($json);
            } else {
               $this->get('session')->getFlashBag()->add('alert alert-success', $trans->trans('js_datos_guardados', array(), 'corerror'));
               return $this->redirectToRoute('<?= $route_name ?>_index', array('slugSelected' => $request->get('slugSelected')));
            }

        }
        elseif($form->isSubmitted()) {
          if ($isAjax) {
            $json['status'] = 0;
            $json['error'] = $this->getFormErrorMessages($form);
            return new JsonResponse($json);
          } else {
                $this->get('session')->getFlashBag()->add('alert alert-danger', $trans->trans('js_datos_no_guardados', array(), 'corerror'));
             }
          }

        return $this->renderQbabitScope('<?= $pathTwigView ? $pathTwigView : $route_name ?>/new.html.twig', 'backend', [
        '<?= $entity_twig_var_singular ?>' => $<?= $entity_var_singular ?>,
        'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{<?= $entity_identifier ?>}", name="<?= $route_name ?>_show", methods="GET")
     */
    public function show<?= $prefixAction ?>(<?= $entity_class_name ?> $<?= $entity_var_singular ?>): Response
    {
        return $this->render('<?= $pathTwigView ? $pathTwigView : $route_name ?>/show.html.twig', ['<?= $entity_twig_var_singular ?>' => $<?= $entity_var_singular ?>]);
    }

    /**
     * @Route("/{<?= $entity_identifier ?>}/edit", name="<?= $route_name ?>_edit", methods="GET|POST")
     */
    public function edit<?= $prefixAction ?>(Request $request, <?= $entity_class_name ?> $<?= $entity_var_singular ?>): Response
    {

        $isAjax = $request->isXmlHttpRequest();
        $json = array('status' => 1, 'error' => array(), 'stringError'=>'');
        $trans = $this->get('translator');

        if(!$this->isGranted('ROLE_X_EDIT')){
        throw $this->createAccessDeniedException();
        }

        $form = $this->createForm(<?= $form_class_name ?>::class, $<?= $entity_var_singular ?>, [
           'method'=>'POST',
           'action'=>$this->generateUrl('<?= $route_name ?>_edit', array('slugSelected' => $request->get('slugSelected'), '<?= $entity_identifier ?>' => $<?= $entity_var_singular ?>->get<?= ucfirst($entity_identifier) ?>()))
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            if ($isAjax) {
              return new JsonResponse($json);
            } else {
              $this->get('session')->getFlashBag()->add('alert alert-success', $trans->trans('js_datos_guardados', array(), 'corerror'));
              return $this->redirectToRoute('<?= $route_name ?>_edit', array('slugSelected' => $request->get('slugSelected'), '<?= $entity_identifier ?>' => $<?= $entity_var_singular ?>->get<?= ucfirst($entity_identifier) ?>()));
            }
        }
        elseif($form->isSubmitted()) {
          if ($isAjax) {
            $json['status'] = 0;
            $json['error'] = $this->getFormErrorMessages($form);
            return new JsonResponse($json);
           } else {
              $this->get('session')->getFlashBag()->add('alert alert-danger', $trans->trans('js_datos_no_guardados', array(), 'corerror'));
             }
          }

        return $this->renderQbabitScope('<?= $pathTwigView ? $pathTwigView : $route_name ?>/edit.html.twig', 'backend', [
        '<?= $entity_twig_var_singular ?>' => $<?= $entity_var_singular ?>,
        'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete", name="<?= $route_name ?>_delete", methods="DELETE")
     */
    public function delete<?= $prefixAction ?>(Request $request): Response
    {
        $json = array('status' => 1, 'error' => array(), 'stringError'=>'');

        if(!$this->isGranted('ROLE_X_DELETE')){
          throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();
        $listIds = $request->get('idcheck', array());
        $repo = $em->getRepository(<?= $entity_class_name ?>::class);
        foreach ($listIds as $id) {
           $entity = $repo->find($id);
           if ($entity) {
             try {
               $em->remove($entity);
               $em->flush();
               $json['ids'][] = $id;
             } catch (\Exception $e) {}
           }
        }

        if (!count($json['ids'])){
          $json['status'] = 0;
        }

        return new JsonResponse($json);
    }
}
